#ifndef LDNN_CPU_H_
#define LDNN_CPU_H_

#include "vl/kmeans.h"
#include <time.h>
#include <math.h>
#include <iostream>
#include <cstring>
#include <vector>

const int BSIZE = 100;

////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////

template< typename T >
int shuffle(T* array1, int dim1, int size){

	T temp;
	double random;
	srand( time(NULL) );

	for(long int i = 0 ; i < size ; i++){
		random = drand48();
		long int index = random * size; ;
		for(long int j = 0 ; j < dim1 ; j++){
			temp = array1[i * dim1 + j];
			array1[i * dim1 + j] = array1[index * dim1 + j];
			array1[index * dim1 + j] = temp;
		}
	}

	return 0;
}

////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////

template< typename T1, typename T2 >
int shuffle(T1* array1, T2* array2, int dim1, int dim2, int size){

	T1 temp1;
	T2 temp2;
	double random;
	srand( time(NULL) );

	for(long int i = 0 ; i < size ; i++){
		random = drand48();
		long int index = random*size; ;
		for(long int j = 0 ; j < dim1 ; j++){
			temp1 = array1[i * dim1 + j];
			array1[i * dim1 + j] = array1[index * dim1 + j];
			array1[index * dim1 + j] = temp1;
		}
		for(long int j = 0 ; j<dim2 ; j++){
			temp2 = array2[i * dim2 + j];
			array2[i * dim2 + j] = array2[index * dim2 + j];
			array2[index * dim2 + j] = temp2;
		}
	}

	return 0;
}

////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////

float* initializeDiscriminants(float* xtrain, float* ytrain, int nDimension,
int nGroup, int nDiscriminant, int nData){

	shuffle(xtrain, ytrain, nDimension, 1, nData);

    int num_P = 0;
    int num_N = 0;
    int downsampleFactor = 1;

    if(nData > 20000000)
    	downsampleFactor = 30;
    else if(nData > 1000000)
    	downsampleFactor = 20;
	else if(nData > 5000000)
		downsampleFactor = 10;
	else if(nData > 2000000)
		downsampleFactor = 5;
	else if(nData > 1000000)
		downsampleFactor = 2;

    for(long int i = 0 ; i < nData ; i++)
        	if(ytrain[i] == 0) num_N++; else num_P++;

    float* c_P = (float*)vl_malloc(nGroup * nDimension * sizeof(float));
    float* c_N = (float*)vl_malloc(nDiscriminant * nDimension * sizeof(float));

    float* data_N;
    float* data_P;

    data_N = (float*)vl_malloc(((num_N / downsampleFactor) + 1) * nDimension *
		sizeof(float));
    data_P = (float*)vl_malloc(((num_P / downsampleFactor) + 1) * nDimension *
	    sizeof(float));

    num_P = 0;
    num_N = 0;

    int cnt_P = 0;
    int cnt_N = 0;

    for(long int i = 0 ; i < nData ; i++){
    	if(ytrain[i] == 0){
    		if(num_N%downsampleFactor == 0){
    			for(long int j = 0 ; j < nDimension ; j++)
    				data_N[cnt_N * nDimension + j] = xtrain[i * nDimension + j];
    			cnt_N++;
    		}
    		num_N++;
    	}else if(ytrain[i] == 1){
    		if(num_P%downsampleFactor == 0){
    			for(long int j = 0 ; j < nDimension ; j++)
    				data_P[cnt_P * nDimension + j] = xtrain[i * nDimension + j];
    			cnt_P++;
    		}
    		num_P++;
   		}
    }

	std::cout << "Run Clustering ..." << std::endl;
	clock_t t_start, t_end;
	t_start = clock();

	VlKMeans* kmeans_P;
	VlKMeans* kmeans_N;

	kmeans_P = vl_kmeans_new(VL_TYPE_FLOAT, VlDistanceL2);
	kmeans_N = vl_kmeans_new(VL_TYPE_FLOAT, VlDistanceL2);

	vl_kmeans_set_algorithm (kmeans_P, VlKMeansLloyd);
	vl_kmeans_set_algorithm (kmeans_N, VlKMeansLloyd);
	
	vl_kmeans_init_centers_plus_plus(kmeans_P, data_P, nDimension, cnt_P,
	nGroup);
	vl_kmeans_init_centers_plus_plus(kmeans_N, data_N, nDimension, cnt_N,
	nDiscriminant);

	vl_kmeans_set_max_num_iterations (kmeans_P, 100);
	vl_kmeans_set_max_num_iterations (kmeans_N, 100);

	vl_kmeans_refine_centers (kmeans_P, data_P, cnt_P);
	vl_kmeans_refine_centers (kmeans_N, data_N, cnt_N);
	
    t_end = clock();

    float time = float(t_end - t_start)/CLOCKS_PER_SEC;

	std::cout << "Done, it took " << time << " seconds!" << std::endl;

	c_P = (float*)vl_kmeans_get_centers(kmeans_P);
	c_N = (float*)vl_kmeans_get_centers(kmeans_N);

	vl_free(data_N);
	vl_free(data_P);

	float* h_discriminants = (float*)malloc(nGroup * nDiscriminant *
		(nDimension + 1) * sizeof(float));
    float* sum_discriminants = (float*)calloc(nGroup * nDiscriminant,
	sizeof(float));
    float* bias = (float*)calloc(nGroup * nDiscriminant, sizeof(float));

    for(long int i = 0 ; i < nGroup ; i++){
    	for(long int j = 0 ; j < nDiscriminant ; j++){
    		for(long int k = 0 ; k < nDimension ; k++){
    			h_discriminants[i * (nDimension + 1) * nDiscriminant + j *
					(nDimension + 1) + k] = (c_P[i * nDimension + k] - c_N[j *
						nDimension + k]);
    			sum_discriminants[i * nDiscriminant + j] += powf((c_P[i *
					nDimension + k] - c_N[j * nDimension + k]), 2);
    			bias[i * nDiscriminant + j] -= (c_P[i * nDimension + k] +
					c_N[j * nDimension + k]) * 0.5 * (c_P[i * nDimension + k] -
						c_N[j * nDimension + k]);
    		}
    	}
    }

    for(long int i = 0 ; i < nGroup ; i++){
    	for(long int j = 0 ; j < nDiscriminant ; j++){
    		if(sqrt(sum_discriminants[i * nDiscriminant + j]) !=0 ){
    	    	for(long int k = 0 ; k < nDimension + 1; k++){
        			if(k == nDimension)
        				h_discriminants[i * (nDimension + 1) * nDiscriminant +
							j * (nDimension + 1) + k] = bias[i *
								nDiscriminant + j] / sqrt(sum_discriminants[i *
									nDiscriminant + j]);
        			else
        				h_discriminants[i * (nDimension + 1) * nDiscriminant +
							j * (nDimension + 1) + k] /=
								sqrt(sum_discriminants[i * nDiscriminant + j]);
    			}
    		}else{
    			std::cout << "Warning:" << std::endl <<
					"	There are zero initialized discriminants"
					<< " ,This may result in problems" << std::endl;
    		}
    	}
    }

    vl_free(c_N);
    vl_free(c_P);
    free(sum_discriminants);
    free(bias);

	return h_discriminants;
}

////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////

float computeAndProduct(float* array, int index, int num){

	int j;
	float result = 1.0f;

	if(index == 0)
		for(j = 1 ; j < num ; j++)
			result *= array[j];
	else if(index == num - 1)
		for(j = 0; j < num - 1 ; j++)
			result *= array[j];
	else{
		for(j = 0 ; j < index ; j++)
			result *= array[j];
		for(j = index + 1 ; j < num ; j++)
			result *= array[j];
	}

	return result;
}

////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////

float* LDNN_train_CPU(float* xtrain, float* ytrain, long int nDimension, 
long int nData, int nGroup, int nDiscriminant, float epsilon, float moment, 
int nEpoch){

	float* discriminants;
	discriminants = initializeDiscriminants(xtrain, ytrain, nDimension,
	nGroup, nDiscriminant, nData);

	float* updates = (float*)calloc((nDimension + 1) * nGroup * nDiscriminant,
	sizeof(float));

	float* prev_updates = (float*)calloc((nDimension + 1) * nGroup *
		nDiscriminant, sizeof(float));
	float* totalError = new float[nEpoch];
	float* discOutputs = new float[nGroup * nDiscriminant];
	float* termOutputs = new float[nGroup * nDiscriminant];
	float* discOutputs_c = new float[nGroup * nDiscriminant];
	float* outputsAnd = new float[nGroup];
	float* outputsAnd_c = new float[nGroup];
	float* h_xtrain = new float[nDimension + 1];

	float time;
	time_t end1, start1;
	time_t end2, start2 = clock();

	// Starting the backpropagation

	float sum_d_pts;
	float and_c_muls;
	float mul_d_pts;
	float output;
	float error;
	float temp;

	int i, j, k, e;

	for(e = 0 ; e < nEpoch ; e++){

		totalError[e] = 0;
		start1 = clock();
		shuffle(xtrain, ytrain, nDimension, 1, nData);
		std::cout << "Starting Epoch: " << e + 1 << std::endl;

		for(i = 0 ; i < nData ; i++){

			memcpy(h_xtrain, &xtrain[i * nDimension], nDimension * 
			sizeof(float));
			h_xtrain[nDimension] = 1;


			for(j = 0 ; j < nGroup * nDiscriminant ; j++){

				sum_d_pts = 0;

				for(k = 0 ; k < nDimension + 1 ; k++){
					sum_d_pts += discriminants[j * (nDimension + 1) + k]
						* h_xtrain[k];
				}

				discOutputs[j] = 1 / (1 + exp(-sum_d_pts));
				discOutputs_c[j] = 1 - discOutputs[j];

			}

			and_c_muls = 1;

			for(j = 0 ; j < nGroup ; j++){

				mul_d_pts = 1;

				for(int k = 0 ; k < nDiscriminant ; k++){
					mul_d_pts *= discOutputs[j * nDiscriminant + k];
				}

				outputsAnd[j] = mul_d_pts;
				outputsAnd_c[j] = 1 - mul_d_pts;
				and_c_muls *= outputsAnd_c[j];

			}

			output = 1 - and_c_muls;
			error = 0.1 + 0.8 * ytrain[i] - output;
			totalError[e] += error * error;

			for(j = 0 ; j < nGroup ; j++){

				temp = computeAndProduct(outputsAnd_c, j, nGroup);

				for(k = 0 ; k < nDiscriminant ; k++){
					termOutputs[j * nDiscriminant + k] = temp * error *
						outputsAnd[j] * discOutputs_c[j * nDiscriminant + k];
				}

			}

			for(j = 0 ; j < nGroup * nDiscriminant ; j++){
				for(k = 0 ; k < nDimension + 1 ; k++){

					updates[j * (nDimension + 1) + k] += h_xtrain[k] * 
					termOutputs[j];

					if((i+1)%BSIZE == 0){

						discriminants[j * (nDimension + 1) + k] += epsilon *
								(updates[j * (nDimension + 1) + k] + moment *
										prev_updates[j * (nDimension + 1) + k]);
						prev_updates[j * (nDimension + 1) + k] =
							updates[j * (nDimension + 1) + k] + moment *
								prev_updates[j * (nDimension + 1) + k];
						updates[j * (nDimension + 1) + k] = 0;

					}
				}
			}
		}

		totalError[e] = sqrt(totalError[e]/nData);
		std::cout << "Total error for epoch " << e + 1 << " is: -----------> "
			<< totalError[e] << std::endl;
		end1 = clock();
		time = float(end1 - start1) / CLOCKS_PER_SEC;
		std::cout << "This epoch took " << time << " seconds to finish."
			<< std::endl;
	}
		end2 = clock();
		time = float(end2 - start2) / CLOCKS_PER_SEC;
		std::cout << "The whole backpropagation took " << time <<
			" seconds to finish." << std::endl;
			
	delete[] totalError;
	delete[] discOutputs;
	delete[] termOutputs;
	delete[] discOutputs_c;
	delete[] outputsAnd;
	delete[] outputsAnd_c;
	delete[] h_xtrain;	

	return discriminants;
}

////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////

float* LDNN_predict_CPU(float* xtrain, float* discriminants, int nDimension, 
int nData, int nGroup, int nDiscriminant){
	
	float* predict = new float[nData];
	float* discOutputs = new float[nGroup * nDiscriminant];
	float* outputsAnd_c = new float[nGroup];
	float* h_xtrain = new float[nDimension + 1];

	float sum_d_pts;
	float and_c_muls;
	float mul_d_pts;
	float output;
	
	int i, j, k;

	for(i = 0 ; i < nData ; i++){

		memcpy(h_xtrain, &xtrain[i * nDimension], nDimension * sizeof(float));
		h_xtrain[nDimension] = 1;

		for(j = 0 ; j < nGroup * nDiscriminant ; j++){
			
			sum_d_pts = 0;

			for(k = 0 ; k < nDimension + 1 ; k++){
				sum_d_pts += discriminants[j * (nDimension + 1) + k]
					* h_xtrain[k];
			}

			discOutputs[j] = 1 / (1 + exp(-sum_d_pts));

		}

		and_c_muls = 1;

		for(j = 0 ; j < nGroup ; j++){

			mul_d_pts = 1;

			for(int k = 0 ; k < nDiscriminant ; k++){
				mul_d_pts *= discOutputs[j * nDiscriminant + k];
			}

			outputsAnd_c[j] = 1 - mul_d_pts;
			and_c_muls *= outputsAnd_c[j];

		}
		
		output = 1 - and_c_muls;
		predict[i] = output;
	}
	
	delete[] discOutputs;
	delete[] outputsAnd_c;
	delete[] h_xtrain;

	return predict;
}

	
template < typename TPixelType >
void subSample(std::vector< std::vector< TPixelType > > &matrix,
std::vector< TPixelType > &vector, const int numP, const int numN, 
std::vector< std::vector< TPixelType > > &n_matrix, std::vector
< TPixelType > &n_vector){
	
	int pixP = 0;
	int pixN = 0;
		
	int num;
	int cntP = 0;
	int cntN = 0;
	int cnt = 0;
	
	int* indexP;
	int* indexN;
		
	typename std::vector< TPixelType >::iterator it;
	
	if(numN + numP > maxNumberOfSamples){

		for(it = vector.begin() ; it != vector.end() ; ++it ){
			if(*it == 0)
				pixN++;
			else
				pixP++;		
		}
				
		indexP = new int[pixP];		
		indexN = new int[pixN];

		for(it = vector.begin() ; it != vector.end() ; ++it ){
			if(*it == 0){
				indexN[cntN] = cnt; 
				cntN++;
			}
			else{
				indexP[cntP] = cnt; 
				cntP++;
			}
			cnt++;		
		}
		
		num = static_cast<int>(maxNumberOfSamples * static_cast<float>
		(pixN + pixP) / static_cast<float>(numN + numP));

		int minP;
		int minN;
		
		if(pixN >= num / 2 && pixP >= num / 2){
			minP = num / 2;
			minN = num / 2;
		}
		else if(pixN >= num / 2 && pixP < num / 2){
			minP = pixP;
			minN = num - pixP;
		}
		else if(pixN < num / 2 && pixP >= num / 2){
			minN = pixN;
			minP = num - pixN;
		}
		std::vector< TPixelType > temp(matrix[0].size(), 0);
		n_matrix = std::vector< std::vector< TPixelType > >(minN + minP, temp);
		n_vector = std::vector< TPixelType >(minN + minP, 0);
		
		shuffle(indexP, 1, cntP);	
		shuffle(indexN, 1, cntN);
		
		for(int i = 0 ; i < minN ; i++){	
			n_matrix[i] = matrix[indexN[i]];
			n_vector[i] = vector[indexN[i]];
		}
		
		for(int i = 0 ; i < minP ; i++){
			n_matrix[i + minN] = matrix[indexP[i]];
			n_vector[i + minN] = vector[indexP[i]];
		}
		
		delete[] indexN;
		delete[] indexP;

	}
	else{
		/*n_matrix.clear();
		n_vector.clear();
		n_matrix.reserve(matrix.size());
		n_vector.reserve(vector.size());
		std::copy(matrix.begin(), matrix.end(), n_matrix.begin());
		std::copy(vector.begin(), vector.end(), n_vector.begin());
		*/
		n_matrix = matrix;
		n_vector = vector;
	}


}

#endif















