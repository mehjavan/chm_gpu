cmake_minimum_required(VERSION 2.8)

project(Image)

INCLUDE_DIRECTORIES( ${MATLAB_INC_DIR} ${VL_INC_DIR})
LINK_DIRECTORIES(${MATLAB_LIB_DIR} ${VL_LIB_DIR} )

set(IMAGE_LINKS mat mx vl)

list( APPEND CMAKE_CXX_FLAGS "-std=c++0x ${CMAKE_CXX_FLAGS} -g -ftest-coverage -fprofile-arcs" )

find_package(ITK REQUIRED)
include(${ITK_USE_FILE})
 
add_library(Image STATIC ImageIO.hpp ImageAlgorithm.hpp ImageFeature.hpp)
target_link_libraries(Image ${ITK_LIBRARIES} ${IMAGE_LINKS})
