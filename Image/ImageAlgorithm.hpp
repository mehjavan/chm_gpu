#ifndef _IMAGE_ALGORITHM_H
#define _IMAGE_ALGORITHM_H
// ITK include files
#include "itkImage.h"
#include "itkImageRegionConstIterator.h"
#include "itkIdentityTransform.h"
#include "itkResampleImageFilter.h"
#include "itkBSplineInterpolateImageFunction.h"
#include "itkDiscreteGaussianImageFilter.h"
#include "itkMirrorPadImageFilter.h"
#include "itkRegionOfInterestImageFilter.h"
#include "itkExtractImageFilter.h"
#include "itkImageRegionIterator.h"
#include "itkNeighborhoodAlgorithm.h"
#include "itkConstantPadImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkConstantBoundaryCondition.h"
#include "itkWindowedSincInterpolateImageFunction.h"
#include "itkAffineTransform.h"

namespace meh{

	///////////////////////////////////////////////////////////////////////////
	// Filter an ITK image with a Gaussian filter
	///////////////////////////////////////////////////////////////////////////
	
	template < typename TImageType, typename TImageType2 = TImageType >
	typename TImageType2::Pointer gaussianFilter(const typename 
	TImageType::Pointer &image, double variance){
		
		typedef itk::DiscreteGaussianImageFilter< TImageType, TImageType2 > 
			filterType;
		typename filterType::Pointer gaussianFilter = filterType::New();
		
		gaussianFilter->SetInput( image );
		gaussianFilter->SetVariance( variance );
		gaussianFilter->Update();
				
		return gaussianFilter->GetOutput();		
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Resize an ITK image
	///////////////////////////////////////////////////////////////////////////
	
	template < typename TPixelType, int Dimension = 2 >
	typename itk::Image< TPixelType, Dimension >::Pointer resizeImage(
	const typename itk::Image< TPixelType, Dimension >::Pointer &image, 
	size_t new_height, size_t new_width, size_t new_depth = 1, 
	double variance = 1.0){
			 
		typedef itk::Image< TPixelType, Dimension > ImageType;	
		
		typename ImageType::RegionType originalRegion = 
		image->GetLargestPossibleRegion();
		
		typename ImageType::SizeType originalSize = originalRegion.GetSize(); 
			 
		if(originalSize[0] == new_width && originalSize[1] == new_height){
			if(Dimension == 2)
				return image;
			else if (originalSize[2] == new_depth)
				return image;
		}	 
			 
		typename ImageType::Pointer filteredImage = ImageType::New();
		filteredImage = meh::gaussianFilter< ImageType, ImageType>(image, 
		variance);
		
		typename ImageType::SizeType inputSize = 
			filteredImage->GetLargestPossibleRegion().GetSize();
		
		typename ImageType::SizeType outputSize;
		
		outputSize[0] = new_width;
		outputSize[1] = new_height;
		if(Dimension == 3)
			outputSize[2] = new_depth;
		
		typename ImageType::SpacingType outputSpacing;

		outputSpacing[0] = filteredImage->GetSpacing()[0] * (
			static_cast<double>(inputSize[0]) / 
				static_cast<double>(outputSize[0]));
		outputSpacing[1] = filteredImage->GetSpacing()[1] * (
			static_cast<double>(inputSize[1]) / 
				static_cast<double>(outputSize[1]));
		if(Dimension == 3){
		    outputSpacing[2] = filteredImage->GetSpacing()[2] * (
				static_cast<double>(inputSize[2]) / 
					static_cast<double>(outputSize[2]));
		}
		
		typedef itk::ConstantBoundaryCondition< ImageType >
                                                        BoundaryConditionType;
		const unsigned int WindowRadius = 5;
		typedef itk::Function::HammingWindowFunction< WindowRadius > 
		WindowFunctionType;
		typedef itk::WindowedSincInterpolateImageFunction< ImageType, WindowRadius, WindowFunctionType,
		    BoundaryConditionType, double  > InterpolatorType;
		typename InterpolatorType::Pointer interpolator = InterpolatorType::New();
		
		/*typedef itk::BSplineInterpolateImageFunction< ImageType, 
		double, double > InterpolatorType;
		
		typename InterpolatorType::Pointer interpolator = 
			InterpolatorType::New();
		interpolator->SetSplineOrder(3);
		*/
		
		typedef itk::AffineTransform< double, Dimension > TransformType;
		typedef itk::ResampleImageFilter< ImageType, ImageType > 
			ResampleImageFilterType;
		typename ResampleImageFilterType::Pointer 
			resample = ResampleImageFilterType::New();
		
		if(variance == 0)
			resample->SetInput(image);
		else
			resample->SetInput(filteredImage);

		resample->SetSize(outputSize);
		resample->SetOutputSpacing(outputSpacing);
		resample->SetTransform(TransformType::New());
		resample->SetInterpolator(interpolator);
		resample->UpdateLargestPossibleRegion();
		
		return resample->GetOutput();
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Mirror Pad an ITK image
	///////////////////////////////////////////////////////////////////////////
	
	template < typename TPixelType, int Dimension = 2 >
	typename itk::Image< TPixelType, Dimension >::Pointer mirrorPadImage(
		const typename itk::Image< TPixelType, Dimension >::Pointer &image, 
	size_t left, size_t top, size_t back = 0, size_t right = -1, 
	size_t bottom = -1, size_t front = -1){
		
		typedef itk::Image< TPixelType, Dimension > ImageType;
		typedef itk::MirrorPadImageFilter < ImageType, ImageType > 
			MirrorPadImageFilterType;
		
		typename ImageType::SizeType lowerBound;
		
		lowerBound[0] = left;
		lowerBound[1] = top;
		if(Dimension == 3)
			lowerBound[2] = back;
 	   
		typename ImageType::SizeType upperBound;
		
		upperBound[0] = ((right == -1) ? left : right);
		upperBound[1] = ((bottom == -1) ? top : bottom);
		if(Dimension == 3)
			upperBound[2] = ((front == -1) ? back : front);
		
		typename MirrorPadImageFilterType::Pointer padFilter
	            = MirrorPadImageFilterType::New();
		padFilter->SetInput(image);
		padFilter->SetPadLowerBound(lowerBound);
		padFilter->SetPadUpperBound(upperBound);
		padFilter->Update();
		
		return padFilter->GetOutput();
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Extract Region of Interest of an ITK image
	///////////////////////////////////////////////////////////////////////////
	
	template < typename TImageType, typename TImageType2 = TImageType >
	typename TImageType2::Pointer regionOfInterestImage(const typename 
	TImageType::Pointer &image, size_t startx, size_t starty, size_t sizex,
	size_t sizey){
		
		typename TImageType::IndexType start;
		start[0] = startx;
		start[1] = starty;

		typename TImageType::SizeType size;
		size[0] = sizex;
		size[1] = sizey;

		typename TImageType::RegionType region;
		region.SetSize( size );
		region.SetIndex( start );
		
		typedef itk::RegionOfInterestImageFilter< TImageType, TImageType2 >
		  FilterType;
		typename FilterType::Pointer filter = FilterType::New();
		filter->SetRegionOfInterest( region );
		filter->SetInput( image );
		
		filter->Update();
		
		return filter->GetOutput();
	}
	
	
	///////////////////////////////////////////////////////////////////////////
	// Extract Region of an ITK image
	///////////////////////////////////////////////////////////////////////////
	
	template < typename TImageType, typename TImageType2 = TImageType >
	typename TImageType2::Pointer extractRegion(const typename 
	TImageType::Pointer &image, size_t startx, size_t starty, size_t sizex,
	size_t sizey){
		
		typename TImageType::IndexType start;
		start[0] = startx;
		start[1] = starty;

		typename TImageType::SizeType size;
		size[0] = sizex;
		size[1] = sizey;

		typename TImageType::RegionType region(start, size);

		typedef itk::ExtractImageFilter< TImageType, TImageType2 > FilterType;
		typename FilterType::Pointer filter = FilterType::New();
		filter->SetExtractionRegion(region);
		filter->SetInput(image);
		filter->SetDirectionCollapseToIdentity(); 
		filter->Update();
		
		return filter->GetOutput();
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Constant pad an itk image
	///////////////////////////////////////////////////////////////////////////
	
	template < typename TPixelType , int Dimension = 2 >
	typename itk::Image< TPixelType, Dimension >::Pointer constPadImage(
		const typename itk::Image< TPixelType, Dimension >::Pointer &image, 
	size_t left, size_t top, size_t back = 0, size_t right = -1, 
	size_t bottom = -1, size_t front = -1, int value = 0){
		
		typedef itk::Image< TPixelType, Dimension > ImageType;
		
		typedef itk::ConstantPadImageFilter < ImageType, ImageType > 
			PadImageFilterType;
		
		typename ImageType::SizeType lowerBound;
		
		lowerBound[0] = left;
		lowerBound[1] = top;
		if(Dimension == 3)
			lowerBound[2] = back;
 
		typename ImageType::SizeType upperBound;
		
		upperBound[0] = ((right == -1) ? left : right);
		upperBound[1] = ((bottom == -1) ? top : bottom);
		if(Dimension == 3)
			upperBound[2] = ((front == -1) ? back : front);
		
		typename PadImageFilterType::Pointer padFilter
	            = PadImageFilterType::New();
		padFilter->SetInput(image);
		padFilter->SetPadLowerBound(lowerBound);
		padFilter->SetPadUpperBound(upperBound);
		padFilter->SetConstant(value);
		padFilter->Update();
		
		return padFilter->GetOutput();
		
	}

	///////////////////////////////////////////////////////////////////////////
	// Maxpool an itk image
	///////////////////////////////////////////////////////////////////////////
	
	template < typename TPixelType , int Dimension = 2 >
	typename itk::Image< TPixelType, Dimension >::Pointer maxPoolImage(
		const typename itk::Image< TPixelType, Dimension >::Pointer &image,
	int level){
		
		if(level == 0)
			return image;
		
		// Getting the size of the original image
		
		typedef itk::Image< TPixelType, Dimension > ImageType;
		typename ImageType::RegionType t_region = 
			image->GetLargestPossibleRegion();
		
		typename ImageType::SizeType size = t_region.GetSize();
		
		int width = size[0];
		int height = size[1];
		int depth = 1;
		if(Dimension == 3)
			depth = size[2];	
		
		// Finding the pooled image size
		
		int n_width = int((width + pow(2, level) - 1) / pow(2, level));
		int n_height = int((height + pow(2, level) - 1) / pow(2, level));
		int n_depth = int((depth + pow(2, level) - 1) / pow(2, level));
		
		// Finding the amount of paddding needed and padding accordingly
		
		int band = powf(2, level);
		
		int pad_width = n_width * powf(2, level) - width;
		int pad_height = n_height * powf(2, level) - height;
		int pad_depth = n_width * powf(2, level) - depth;
		
		typename ImageType::Pointer paddedImage = ImageType::New();
		paddedImage = meh::constPadImage< TPixelType, Dimension >
			(image, 0, 0, 0, pad_width, pad_height, pad_depth);
		
		// Defining the region extraction filter
		
		typename ImageType::IndexType cellStart;
		typename ImageType::SizeType cellSize;

		cellSize[0] = band;
		cellSize[1] = band;
		if(Dimension == 3)
			cellSize[2] = band;

		typename ImageType::RegionType region;
		region.SetSize(cellSize);

		typedef itk::ExtractImageFilter< ImageType, ImageType > FilterType;
		typename FilterType::Pointer filter = FilterType::New();

		filter->SetInput(paddedImage);
		filter->SetDirectionCollapseToIdentity(); 
		
		// Creating pooled image
		
		typename ImageType::RegionType n_region;
		typename ImageType::IndexType startImage;
		
		startImage[0] = 0;
		startImage[1] = 0;
		if(Dimension == 3)
			startImage[2] = 0;
 
		typename ImageType::SizeType sizeImage;
		
		sizeImage[0] = n_width;
		sizeImage[1] = n_height;
		if(Dimension == 3)
			sizeImage[2] = n_depth;
 
		n_region.SetSize(sizeImage);
		n_region.SetIndex(startImage);
 
		typename ImageType::Pointer pooledImage = ImageType::New();
		pooledImage->SetRegions(n_region);
		pooledImage->Allocate();
		
		itk::ImageRegionIterator< ImageType > 
			imageIterator(pooledImage, n_region);
		imageIterator.GoToBegin();
		
		// Defining max value filter
		
		typedef itk::MinimumMaximumImageCalculator < ImageType >
		          ImageCalculatorFilterType;
		typename ImageCalculatorFilterType::Pointer imageCalculatorFilter
	            = ImageCalculatorFilterType::New ();
		
		// Max Pooling
		
		for(int k = 0 ; k < n_depth ; k++){
			for(int j = 0 ; j < n_height ; j++){
				for(int i = 0 ; i < n_width ; i++){

				    cellStart[0] = i * band;
				    cellStart[1] = j * band;
					if(Dimension == 3)
						cellStart[2] = k * band;
		
					region.SetIndex(cellStart);
					
				    filter->SetExtractionRegion(region);			
				    filter->UpdateLargestPossibleRegion();
					
					imageCalculatorFilter->SetImage(filter->GetOutput());
					imageCalculatorFilter->Compute();
					
					imageIterator.Set(imageCalculatorFilter->GetMaximum());
					++imageIterator;
					
				}
			}
		}
		
		return pooledImage;
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Downsample an itk image
	///////////////////////////////////////////////////////////////////////////
	
	template < typename TPixelType, int Dimension = 2 >
	typename itk::Image< TPixelType, Dimension >::Pointer downsampleImage(
		const typename itk::Image< TPixelType, Dimension >::Pointer &image,
	int level){
		
		typedef itk::Image< TPixelType, Dimension > ImageType;
		typename ImageType::RegionType region = 
			image->GetLargestPossibleRegion();
		
		typename ImageType::SizeType size = region.GetSize();
		
		int n_width = int((size[0] + pow(2, level) - 1) / pow(2, level));
		int n_height = int((size[1] + pow(2, level) - 1) / pow(2, level));
		int n_depth = 1;
		if(Dimension == 3)
			n_depth = int((size[2] + pow(2, level) - 1) / pow(2, level));
		
		
		return meh::resizeImage< TPixelType , Dimension >(image, n_width, 
		n_height, n_depth);
		
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Upsample an itk image
	///////////////////////////////////////////////////////////////////////////
	
	template < typename TPixelType, int Dimension = 2 >
	typename itk::Image< TPixelType, Dimension >::Pointer upsampleImage(
		const typename itk::Image< TPixelType, Dimension >::Pointer &image,
	int level){
		
		typedef itk::Image< TPixelType, Dimension > ImageType;
		typename ImageType::RegionType region = 
			image->GetLargestPossibleRegion();
		
		typename ImageType::SizeType size = region.GetSize();
		
		int n_width = size[0] * pow(2, level);
		int n_height = size[1] *  pow(2, level);
		int n_depth = 1;
		if(Dimension == 3)
			n_depth = size[2] * pow(2, level);
		
		return meh::resizeImage< TPixelType , Dimension >(image, n_width, 
		n_height, n_depth);
		
	}
}


#endif

