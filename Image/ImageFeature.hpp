#ifndef _IMAGE_FEATURE_H
#define _IMAGE_FEATURE_H
// ITK include files
#include "itkImage.h"
#include "itkImageRegionConstIterator.h"
#include "itkShapedNeighborhoodIterator.h"
#include "itkImageRegionIterator.h"
#include "itkNeighborhoodAlgorithm.h"


// VLFEAT include files
#include <vl/dsift.h>
#include <vl/generic.h>
#include <vl/hog.h>
#include "ImageIO.hpp"


namespace meh{
  

	///////////////////////////////////////////////////////////////////////////
	// Extract a window from 2D buffer image
	///////////////////////////////////////////////////////////////////////////
	
	template < typename TPixelType >
	void cropBufferImage_2D(TPixelType* image, TPixelType* cropped, size_t 
	width, size_t height, size_t x_1, size_t y_1, size_t x_2, size_t y_2){

		size_t n_width = x_2 - x_1 + 1;
		size_t n_height = y_2 - y_1 + 1;
		
		for(int j = y_1 ; j <= y_2 ; j++){
			for(int i = x_1 ; i <= x_2 ; i++){
				cropped[(i-x_1) * n_height + (j-y_1)] = image[i * height +  j];
			}
		}
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Construct HOG feature of an ITK image
	///////////////////////////////////////////////////////////////////////////
	
	template < typename TPixelType >
	std::vector< std::vector < float > > constructHOG(
	const typename itk::Image< TPixelType, 2 >::Pointer &image,
	size_t numberOfBins = 9, size_t patchRadius = 7 ){
		
		size_t patchSize = 2 * patchRadius + 1;
		
		typedef itk::Image< TPixelType, 2 > ImageType;
		typename ImageType::RegionType region;  
		region = image->GetBufferedRegion();
		
		typename ImageType::SizeType size = region.GetSize();
		
		size_t width = size[0];
		size_t height = size[1];
		
		typename ImageType::Pointer paddedImage = ImageType::New();
		paddedImage = meh::mirrorPadImage< TPixelType >
			(image, patchRadius, patchRadius);
		
		int i, j;
		
		float* buffer = new float[patchSize * patchSize];	
		float* bufferImage;
		//bufferImage = meh::transferImageToBuffer< TPixelType >(paddedImage);
		float* temp = meh::transferImageToBuffer< TPixelType >(paddedImage);
		bufferImage = transposeBufferImage_2(temp, width + patchSize - 1,
						   height + patchSize - 1);
		delete[] temp;
		meh::cropBufferImage_2D(bufferImage, buffer, (width + 2 * patchRadius), 
		(height + 2 * patchRadius), 0, 0, 14, 14);
		
		VlHog* hog = vl_hog_new(VlHogVariantDalalTriggs, numberOfBins, 
		VL_TRUE);
		vl_hog_put_image(hog, buffer, patchSize, patchSize, 1, patchSize) ;
		size_t hogWidth = vl_hog_get_width(hog);
		size_t hogHeight = vl_hog_get_height(hog);
		size_t hogDimension = vl_hog_get_dimension(hog);
		size_t hogSize = hogWidth * hogHeight * hogDimension;
		float* hogArray = (float*)vl_malloc(
			hogWidth * hogHeight * hogDimension * sizeof(float));
		
		std::vector< float > one_feature(hogSize);
		std::vector<std::vector< float > > features(width * height, 
		one_feature);
		
		for(j = 0 ; j < height ; ++j){
			for(i = 0 ; i < width ; ++i){
				meh::cropBufferImage_2D(bufferImage, buffer, (width + 2 * 
					patchRadius), (height + 2 * patchRadius), 
					i, j, i + 14, j + 14);
				vl_hog_put_image(hog, buffer, patchSize, patchSize, 1, 
				patchSize);
				vl_hog_extract(hog, hogArray);
				std::copy(hogArray, hogArray + hogSize, one_feature.begin());
				features[i + j * width] = one_feature;
			}
		}
		
		vl_hog_delete(hog) ;
		vl_free(hogArray);
		delete[] buffer;
		delete[] bufferImage;
		
		return features;
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Construct SIFT features of an ITK image
	///////////////////////////////////////////////////////////////////////////
	
	template < typename TPixelType >
	std::vector< std::vector < float > > constructSIFT(
		const typename itk::Image< TPixelType, 2 >::Pointer &image,
	size_t binSize = 4, size_t step = 1, size_t patchRadius = 6 ){
			
		size_t patchSize = 2 * patchRadius;	
		typedef itk::Image< TPixelType, 2 > ImageType;
		typename ImageType::RegionType region;  
		region = image->GetBufferedRegion();
		
		typename ImageType::SizeType size = region.GetSize();
		
		size_t width = size[0];
		size_t height = size[1];
		
		typename ImageType::Pointer paddedImage = ImageType::New();
		paddedImage = meh::mirrorPadImage< TPixelType > 
			(image, patchRadius, patchRadius);
								
		TPixelType* bufferImage;
		const float* bufferFeatures;
		
		float* temp = meh::transferImageToBuffer< TPixelType >(paddedImage);
		bufferImage = transposeBufferImage_2(temp, width + patchSize, height + patchSize);
		delete[] temp;
		
		//bufferImage = meh::transferImageToBuffer< TPixelType >(paddedImage);
		
		VlDsiftFilter* sift = vl_dsift_new_basic(height + patchSize,
		width + patchSize, step, binSize);
		size_t descriptorSize = vl_dsift_get_descriptor_size(sift);
		size_t numberOfPoints = vl_dsift_get_keypoint_num(sift);
		
		vl_dsift_process(sift, bufferImage);
		bufferFeatures = vl_dsift_get_descriptors(sift);

		int i, j;
		
		std::vector< float > one_feature(descriptorSize);
		std::vector< std::vector< float > > features(numberOfPoints, 
		one_feature);
		
		for(j = 0 ; j < height ; j++){
			for(i = 0 ; i < width ; i++){
				std::copy(bufferFeatures + (i * height + j) * descriptorSize, 
				bufferFeatures + (i * height + j + 1) * descriptorSize, 
				one_feature.begin());
				features[i + j * width] = one_feature;
			}
		}
		
		vl_dsift_delete(sift);		
		delete[] bufferImage;
		
		return features;
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Stencil neighborhood 2D
	///////////////////////////////////////////////////////////////////////////
	
	template< typename TIteratorType >
	std::vector< typename TIteratorType::OffsetType > stencilNeighborhood_2D(
	int radius, int &num){
		
		num = 8 * radius + 1;
		size_t cnt = 0;
		
		std::vector< typename TIteratorType::OffsetType > offset(num);

		for(int i = -radius ; i < radius + 1 ; i++){
			if(i == 0){
				for(int j = -radius ; j < radius + 1 ; j++){
					offset[cnt][0] = 0;
					cnt++;
				}
			}
			else{
				offset[cnt][0] = i;
				offset[cnt+1][0] = i;
				offset[cnt+2][0] = i;
				cnt += 3;
			}
		}

		cnt = 0;

		for(int i = -radius ; i < radius + 1 ; i++){
			if(i == 0){
				for(int j = -radius ; j < radius + 1 ; j++){
					offset[cnt][1] = j;
					cnt++;
				}
			}
			else{
				offset[cnt][1] = i;
				offset[cnt+1][1] = 0;
				offset[cnt+2][1] = -i;
				cnt += 3;
			}
		}
		
		return offset;
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Stencil neighborhood 3D
	///////////////////////////////////////////////////////////////////////////
	
	template< typename TIteratorType >
	std::vector< typename TIteratorType::OffsetType > stencilNeighborhood_3D(
	int radius, int &num){
		
		num = 26 * radius + 1;
		std::vector< typename TIteratorType::OffsetType > offset(num);
		
		int cnt = 0;

		for(int i = -radius ; i < radius + 1 ; i++){
			if(i == 0){
				for(int j = cnt ; j < cnt + 8 * radius + 1 ; j++){
					offset[j][0] = i;
				}
				cnt += 8 * radius + 1;
			}
			else{
				for(int j = cnt ; j < cnt + 9 ; j++){
					offset[j][0] = i;
				}
				cnt += 9;
			}
		}

		cnt = 0;

		for(int i = -radius ; i < radius + 1 ; i++){
			if(i == 0){
				for(int j = -radius ; j < radius + 1 ; j++){
					if(j == 0){
						for(int k = cnt ; k < cnt + 2 * radius + 1 ; k++){
							offset[k][1] = 0;
						}
						cnt += 2*radius + 1;
					}
					else{
						for(int k = cnt ; k < cnt+3 ; k++){
							offset[k][1] = j;
						}
						cnt += 3;
					}
				}
			}
			else{
				for(int j = cnt ; j < cnt + 3 ; j++){
					offset[j][1] = -abs(i);
				}
				cnt += 3;
				for(int j = cnt ; j < cnt + 3 ; j++){
					offset[j][1] = 0;
				}
				cnt += 3;
				for(int j = cnt ; j < cnt + 3 ; j++){
					offset[j][1] = abs(i);
				}
				cnt += 3;
			}
		}

		cnt = 0;

		for(int i = -radius ; i < radius + 1 ; i++){
			if(i == 0){
				for(int j = -radius ; j < radius + 1 ; j++){
					if(j == 0){
						for(int k = cnt ; k < cnt + 2 * radius + 1 ; k++){
							offset[k][2] = k - cnt - radius;
						}
						cnt += 2 * radius + 1;
					}
					else{
						offset[cnt][2] = -abs(j);
						offset[cnt+1][2] = 0;
						offset[cnt+2][2] = abs(j);
						cnt += 3;
					}
				}
			}
			else{
				for(int k = 0 ; k < 3 ; k++){
					offset[cnt][2] = -abs(i);
					offset[cnt+1][2] = 0;
					offset[cnt+2][2] = abs(i);
					cnt += 3;
				}
			}
		}
		
		return offset;
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Extract neighborhood intensity features
	///////////////////////////////////////////////////////////////////////////
	
	template< typename TPixelType, int Dimension = 2 >
	std::vector< std::vector< TPixelType > > extractIntensityFeatures(
	const typename itk::Image< TPixelType, Dimension >::Pointer &image, 
	size_t radiusSize = 10){
		
		typedef itk::Image< TPixelType, Dimension > ImageType;
		typedef itk::ShapedNeighborhoodIterator< ImageType > IteratorType;
		
		typename ImageType::Pointer paddedImage = ImageType::New();
		paddedImage = meh::mirrorPadImage< TPixelType, Dimension >
			(image, radiusSize, radiusSize);
		
		typename ImageType::RegionType region = image->GetBufferedRegion();
		
		typename ImageType::IndexType start;
		start.Fill(0);
		
		typename ImageType::SizeType size = region.GetSize();
		typename ImageType::RegionType regionOfInterest(start, size);
		
		itk::Size< Dimension > radius;
		radius.Fill(radiusSize);
		
		IteratorType iterator(radius, paddedImage, regionOfInterest);
		int num = 0; 
		
		std::vector< typename IteratorType::OffsetType > offset;
		if(Dimension == 2)
			offset = meh::stencilNeighborhood_2D< IteratorType >
				(radiusSize, num);
		else if(Dimension == 3)
			offset = meh::stencilNeighborhood_3D< IteratorType >
				(radiusSize, num);
		
		for(int i = 0 ; i < num ; i++)
			iterator.ActivateOffset(offset[i]);
		
		typename IteratorType::ConstIterator ci;
		
		std::vector< TPixelType > one_feature(num);
		typename std::vector< std::vector< TPixelType > > features(size[0] * 
			size[1], one_feature);
		 
		int i = 0, j;
	    
		for(iterator.GoToBegin() ; !iterator.IsAtEnd() ; ++iterator){
			ci = iterator.Begin();
			j = 0;
	      	while (! ci.IsAtEnd()){
				features[i][j] = ci.Get();
				j++;
		        ci++;
	        }
			i++;
		}
		
		return features;
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Insert 2D std::vectors
	///////////////////////////////////////////////////////////////////////////
	
	template < typename TPixelType >
	void insertVector(std::vector< std::vector
		< TPixelType > > &A, std::vector< std::vector< TPixelType > > B){

		int heightA = A.size();
		int heightB = B.size();
		int widthA = 0;
		int widthB = 0;

		if(heightA == 0){
			A = B;
		}
		else if(heightB != 0) {
			widthB = B[0].size();
			widthA = A[0].size();
			if(heightA != heightB)
				std::cout << "First Dimension should match!" << std::endl;
			for(int i = 0 ; i < heightA ; i++)
				A[i].reserve(widthB + widthA);
		
			for(int i = 0 ; i < heightA ; i++)
				A[i].insert(A[i].end(), B[i].begin(), B[i].end());
		}	
	}
		
	///////////////////////////////////////////////////////////////////////////
	// Concatenate 2D std::vectors
	///////////////////////////////////////////////////////////////////////////

	template < typename TPixelType >
	void catVector(std::vector< std::vector
		< TPixelType > > &A, std::vector< std::vector< TPixelType > > B){
	
		int heightA = A.size();
		int heightB = B.size();
		int widthA = 0;
		int widthB = 0;
			
		if(heightA == 0){
			A = B;
		}
		else if (heightB != 0){
			widthB = B[0].size();
			widthA = A[0].size();
			if(widthA != widthB)
				std::cout << "Second Dimension should match!" << std::endl;
	
			A.reserve(heightB + heightA);
	
			for(int i = 0 ; i < heightB ; i++)
				A.push_back(B[i]);
		}			
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Filter bank a 2D image
	///////////////////////////////////////////////////////////////////////////

	template < typename TPixelType >
	std::vector< std::vector< TPixelType > > filterBankImage( 
	const typename itk::Image< TPixelType, 2 >::Pointer &image){
		
		std::vector< std::vector< float > > hogFeat;
		std::vector< std::vector< float > > siftFeat;
		std::vector< std::vector< float > > intensityFeat;
		
		hogFeat = meh::constructHOG< float >(image);
		siftFeat = meh::constructSIFT< float >(image);
		intensityFeat = meh::extractIntensityFeatures< TPixelType >(image);
		
		meh::insertVector< TPixelType >(hogFeat, siftFeat);
		meh::insertVector< TPixelType >(hogFeat, intensityFeat);
		
		return hogFeat;
	}

	///////////////////////////////////////////////////////////////////////////
	// Filter bank a 2D and 3D image
	///////////////////////////////////////////////////////////////////////////

	template < typename TPixelType, int Dimension = 2 >
	std::vector< std::vector< TPixelType > > contextFilterBankImage( 
		const typename itk::Image< TPixelType, Dimension >::Pointer &image, 
			int radius = 7){
			
		std::vector< std::vector< float > > intensityFeat;
		intensityFeat = meh::extractIntensityFeatures
			< TPixelType, Dimension >(image, radius);
		
		return intensityFeat;
	}

}

#endif
