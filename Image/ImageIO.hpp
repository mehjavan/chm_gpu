#ifndef _IMAGE_IO_H
#define _IMAGE_IO_H
// ITK include files
#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkImportImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkImageRegionConstIterator.h"
#include "itkImageRegionIterator.h"

#include "QuickView.h"

// MATLAB include files
#include "mat.h"
#include "mex.h"

namespace meh{
	
	///////////////////////////////////////////////////////////////////////////
	// Read an image via its address
	///////////////////////////////////////////////////////////////////////////
	
	template < typename TImageType >
	typename TImageType::Pointer readImage( const std::string& address ){
		
		typedef itk::ImageFileReader< TImageType > ReaderType;
		typename ReaderType::Pointer reader = ReaderType::New();
		
		reader->SetFileName( address );
		reader->Update();
		
		return reader->GetOutput();
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Write an image to the given address
	///////////////////////////////////////////////////////////////////////////
	
	template < typename  TImageType >
	void writeImage(typename TImageType::Pointer const& image, 
	std::string const& address){
		
		typedef itk::ImageFileWriter< TImageType > WriterType;
		typename WriterType::Pointer writer = WriterType::New();
		
		writer->SetFileName( address );
		writer->SetInput( image );
		writer->Update();
		
	}
	
	///////////////////////////////////////////////////////////////////////////
	// transpose a 2D buffer array
	///////////////////////////////////////////////////////////////////////////
	
	template < typename TPixelType >
	TPixelType* transposeBufferImage_1(TPixelType* image, long int width, 
	long int height, long int depth = 1){
		
		TPixelType* buffer;
		buffer = new TPixelType[width * height * depth];
		
		for(long int k = 0 ; k < depth ; k++)
			for(long int j = 0 ; j < height ; j++)
				for(long int i = 0 ; i < width ; i++)
					buffer[i + j * width + k * width * height] = 
						image[j + i * height + k * width * height];
				
		
		return buffer;
	}
	
	///////////////////////////////////////////////////////////////////////////
	// transpose a 2D buffer array
	///////////////////////////////////////////////////////////////////////////
	
	template < typename TPixelType >
	TPixelType* transposeBufferImage_2(TPixelType* image, long int width, 
	long int height, long int depth = 1){
		
		TPixelType* buffer;
		buffer = new TPixelType[width * height * depth];
		
		for(long int k = 0 ; k < depth ; k++)
			for(long int j = 0 ; j < height ; j++)
				for(long int i = 0 ; i < width ; i++)
					buffer[j + i * height + k * width * height] = 
						image[i + j * width + k * width * height];
				
		
		return buffer;
	}

	///////////////////////////////////////////////////////////////////////////
	// View an ITK image
	///////////////////////////////////////////////////////////////////////////
	
	template < typename TImageType >
	void showITK(const typename TImageType::Pointer& image){
		
		typedef itk::RescaleIntensityImageFilter< TImageType, 
		TImageType > RescaleFilterType;
		typename RescaleFilterType::Pointer rescaleFilter = 
			RescaleFilterType::New();
		rescaleFilter->SetInput(image);
		rescaleFilter->SetOutputMinimum(0);
		rescaleFilter->SetOutputMaximum(255);
		
		QuickView viewer;
		viewer.AddImage< TImageType >(rescaleFilter->GetOutput());
		viewer.Visualize();		
		
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Print an std::vector in terminal
	///////////////////////////////////////////////////////////////////////////
	
	template <typename TPixelType>
	void printVector(const std::vector< std::vector< TPixelType > > image){
	 
		typename std::vector< std::vector< TPixelType > >::const_iterator irow;
		typename std::vector< TPixelType >::const_iterator icol;
	
		for(irow = image.begin() ; irow != image.end() ; irow++){
			for(icol = irow->begin() ; icol != irow->end() ; icol++)		
				std::cout << *icol << " ";
			std::cout << std::endl; 
		}
	
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Transfer std::vector to ITK image
	///////////////////////////////////////////////////////////////////////////
	
	template< typename TPixelType >
	typename itk::Image< TPixelType, 2 >::Pointer 
	transferVectorToImage_2D(const std::vector< std::vector< TPixelType > > 
	&image){
		
		const int Dimension = 2;
		
		size_t height = image.size();
		size_t width = image[0].size();
		
		TPixelType* buffer = new TPixelType[ height * width];
	
		typename std::vector< std::vector< TPixelType > >::const_iterator irow;
		typename std::vector< TPixelType >::const_iterator icol;
	
		int i, j;
	
		for(i = 0, irow = image.begin() ; irow != image.end() ; irow++, i++){
			for(j = 0, icol = irow->begin() ; icol != irow->end() ; 
			icol++, j++){		
				buffer[i * width + j] = *icol; 
			}
		}
		
		typedef itk::Image< TPixelType, Dimension > ImageType;
		typedef itk::ImportImageFilter< TPixelType, Dimension > 
			ImportFilterType;
		
		typename ImportFilterType::Pointer importFilter = 
			ImportFilterType::New();
		typename ImportFilterType::SizeType size;
		
		size[ 0 ] = width;
		size[ 1 ] = height;
		
		typename ImportFilterType::IndexType start;
		
		start[ 0 ] = 0;
		start[ 1 ] = 0;
		
		typename ImageType::RegionType region;
		region.SetIndex( start );
		region.SetSize( size );
		importFilter->SetRegion( region );
		
		double origin[ Dimension ] = { 0.0, 0.0 };
		importFilter->SetOrigin( origin );
		
		double spacing[ Dimension ] = { 1.0, 1.0 };
		importFilter->SetSpacing( spacing );
		
		const unsigned int numberOfPixels = height * width;
		const bool importImageFilterWillOwnTheBuffer = false;
		importFilter->SetImportPointer( buffer, numberOfPixels,
		importImageFilterWillOwnTheBuffer );
		
		importFilter->Update();
		// Caution
		delete[] buffer;
		return importFilter->GetOutput();
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Transfer ITK image to std::vector
	///////////////////////////////////////////////////////////////////////////
	
	template< typename TPixelType >
	std::vector< std::vector< TPixelType > > transferImageToVector_2D(
	typename itk::Image< TPixelType, 2 >::Pointer &image){
		
		const int Dimension = 2;
		
		typedef itk::Image< TPixelType, Dimension > ImageType;
		typename ImageType::RegionType region;
		
		region = image->GetBufferedRegion();
		
		typename ImageType::SizeType size = region.GetSize();
		typename ImageType::IndexType start = region.GetIndex();
		
		size_t width = size[ 0 ];
		size_t height = size[ 1 ];
			
		
		typedef itk::ImageRegionConstIterator< ImageType > IteratorType;
		IteratorType it( image, region );
		it.GoToBegin();
		
		typename std::vector< std::vector< TPixelType > > i_vector(height,
		typename std::vector< TPixelType > (width));
	
		typename std::vector< std::vector< TPixelType > >::iterator irow;
		typename std::vector< TPixelType >::iterator icol;
	
		int i, j;
	
		while( !it.IsAtEnd() ){
			for(irow = i_vector.begin() ; irow != i_vector.end() ; irow++){
				for(icol = irow->begin() ; icol != irow->end() ; icol++){		
					*icol = it.Get();
					++it; 
				}
			}
		}
		
		return i_vector;
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Transfer ITK image to 1D std::vector
	///////////////////////////////////////////////////////////////////////////

	template< typename TPixelType, int Dimension = 2 >
	std::vector< TPixelType > transferImageTo1DVector(
	const typename itk::Image< TPixelType, Dimension >::Pointer &image){

		typedef itk::Image< TPixelType, Dimension > ImageType;
		typename ImageType::RegionType region;

		region = image->GetBufferedRegion();

		typename ImageType::SizeType size = region.GetSize();
		typename ImageType::IndexType start = region.GetIndex();

		size_t width = size[0];
		size_t height = size[1];
		size_t depth = 1;
		if(Dimension == 3)
			depth = size[2];

		typedef itk::ImageRegionConstIterator< ImageType > IteratorType;
		IteratorType it( image, region );
		it.GoToBegin();

		typename std::vector< TPixelType > i_vector(height * width * depth);
		typename std::vector< TPixelType >::iterator icol;

		int i, j;

		while( !it.IsAtEnd() ){
			for(icol = i_vector.begin() ; icol != i_vector.end() ; icol++){
				*icol = it.Get();
				++it;
			}
		}

		return i_vector;
	}

	///////////////////////////////////////////////////////////////////////////
	// Transfer buffer to ITK image
	///////////////////////////////////////////////////////////////////////////
	
	template< typename TPixelType, int Dimension = 2 >
	typename itk::Image< TPixelType, Dimension >::Pointer 
	transferBufferToImage(TPixelType* buffer, size_t height, size_t width,
	size_t depth = 1){

		typedef itk::Image< TPixelType, Dimension > ImageType;
		typedef itk::ImportImageFilter< TPixelType, Dimension > 
			ImportFilterType;
		
		typename ImportFilterType::Pointer importFilter = 
			ImportFilterType::New();
		typename ImportFilterType::SizeType size;
		
		size[0] = width;
		size[1] = height;
		if(Dimension == 3)
			size[2] = depth;
		
		typename ImportFilterType::IndexType start;
		
		start[0] = 0;
		start[1] = 0;
		if(Dimension == 3)
			start[2] = 0;
		
		typename ImageType::RegionType region;
		region.SetIndex( start );
		region.SetSize( size );
		importFilter->SetRegion( region );

		double origin[ Dimension ] = { 0.0, 0.0 };
		importFilter->SetOrigin( origin );
		
		double spacing[ Dimension ] = { 1.0, 1.0 };
		importFilter->SetSpacing( spacing );
		
		const unsigned int numberOfPixels = height * width * depth;
		const bool importImageFilterWillOwnTheBuffer = false;
		importFilter->SetImportPointer( buffer, numberOfPixels,
		importImageFilterWillOwnTheBuffer );
		
		importFilter->Update();

		return importFilter->GetOutput();
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Transfer ITK image to Buffer
	///////////////////////////////////////////////////////////////////////////
	
	template< typename TPixelType, int Dimension = 2 >
	TPixelType* transferImageToBuffer(
	const typename itk::Image< TPixelType, Dimension >::Pointer &image){
		
		typedef itk::Image< TPixelType, Dimension > ImageType;
		typename ImageType::RegionType region;
		
		region = image->GetBufferedRegion();
		
		typename ImageType::SizeType size = region.GetSize();
		typename ImageType::IndexType start = region.GetIndex();
		
		size_t width = size[0];
		size_t height = size[1];
		size_t depth = 1;
		if(Dimension == 3)
			depth = size[2];;
		
		typedef itk::ImageRegionConstIterator< ImageType > IteratorType;
		IteratorType it( image, region );
		it.GoToBegin();
		
		TPixelType* buffer;
		buffer = new TPixelType[width * height * depth];
		
		TPixelType* temp = buffer;
	
		while( !it.IsAtEnd() ){
			*temp = it.Get();
			++it;
			++temp;
		}
		
		return buffer;
	}
	
	
	///////////////////////////////////////////////////////////////////////////
	// Read MAT file to buffer with No Transpose
	///////////////////////////////////////////////////////////////////////////

	template <typename TPixelType, int Dimension = 2 >
	TPixelType* readMATFileToBuffer_NT(const std::string& address, 
	size_t &height, size_t &width, size_t &depth, const char* name ){

		MATFile *pmat;
		mxArray *pa = 0;
		TPixelType* buffer;
		TPixelType* re_buffer;
		const mwSize *pDims;
		const char* c_address = address.c_str();

		pmat = matOpen(c_address, "r");
		pa = matGetVariable(pmat, name);
		matClose(pmat);

		pDims = mxGetDimensions(pa);

		buffer = (TPixelType*)mxGetData(pa);

		height = pDims[0];
		width = pDims[1];
		depth = 1;
		if(Dimension == 3)
			depth = pDims[2];
		re_buffer = new TPixelType[height * width * depth];	
		//re_buffer = meh::transposeBufferImage< TPixelType >(buffer, width, 
		//height, depth);

		memcpy(re_buffer, buffer, sizeof(TPixelType) * height * width * depth);

		mxFree(buffer);

		return re_buffer;
	}

	
	///////////////////////////////////////////////////////////////////////////
	// Write MAT file from buffer with No Transpose
	///////////////////////////////////////////////////////////////////////////

	template <typename TPixelType, int Dimension = 2 >
	void writeBufferToMATFile_NT(const std::string& address, TPixelType* image, 
	const char* name, size_t height, size_t width, size_t depth = 1){

		MATFile *pmat;
		mxArray *pa = 0;
		const char* c_address = address.c_str();
		mwSize pDims[Dimension];
	
		pDims[0] = height;
		pDims[1] = width;
		pDims[2] = 1;
		if(Dimension == 3)
			pDims[2] = depth;
			
		pa = mxCreateNumericArray(Dimension, pDims, 
		mxSINGLE_CLASS, mxREAL);

		TPixelType *buffer;//, *t_buffer;
		buffer = (TPixelType*)mxMalloc(sizeof(TPixelType) * width * height * depth);
		buffer = (TPixelType*)mxGetData(pa);
		
		//t_buffer = meh::transposeBufferImage< TPixelType >(image, width, 
		//height, depth);
		
		memcpy(buffer, image, sizeof(TPixelType) * width * height * depth);
		
		//delete[] t_buffer;

		pmat = matOpen(c_address, "w");
		matPutVariable(pmat, name, pa);
		matClose(pmat);
		
		mxFree(buffer);
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Read MAT file to buffer
	///////////////////////////////////////////////////////////////////////////

	template <typename TPixelType, int Dimension = 2 >
	TPixelType* readMATFileToBuffer(const std::string& address, 
	size_t &height, size_t &width, size_t &depth, const char* name ){

		MATFile *pmat;
		mxArray *pa = 0;
		TPixelType* buffer;
		TPixelType* re_buffer;
		const mwSize *pDims;
		const char* c_address = address.c_str();

		pmat = matOpen(c_address, "r");
		pa = matGetVariable(pmat, name);
		matClose(pmat);
		
		pDims = mxGetDimensions(pa);
		
		buffer = (TPixelType*)mxGetData(pa);

		height = pDims[0];
		width = pDims[1];
		depth = 1;
		if(Dimension == 3)
			depth = pDims[2];
			
		re_buffer = meh::transposeBufferImage_1< TPixelType >(buffer, width, 
		height, depth);

		mxFree(buffer);
		
		return re_buffer;
	}

	///////////////////////////////////////////////////////////////////////////
	// Write MAT file from buffer
	///////////////////////////////////////////////////////////////////////////

	template <typename TPixelType, int Dimension = 2 >
	void writeBufferToMATFile(const std::string& address, TPixelType* image, 
	const char* name, size_t height, size_t width, size_t depth = 1){

		MATFile *pmat;
		mxArray *pa = 0;
		const char* c_address = address.c_str();
		mwSize pDims[Dimension];
	
		pDims[0] = height;
		pDims[1] = width;
		pDims[2] = 1;
		if(Dimension == 3)
			pDims[2] = depth;
			
		pa = mxCreateNumericArray(Dimension, pDims, 
		mxSINGLE_CLASS, mxREAL);

		TPixelType *buffer, *t_buffer;
		buffer = (TPixelType*)mxMalloc(sizeof(TPixelType) * width * height * depth);
		buffer = (TPixelType*)mxGetData(pa);
		
		t_buffer = meh::transposeBufferImage_2< TPixelType >(image, width, 
		height, depth);
		
		memcpy(buffer, t_buffer, sizeof(TPixelType) * width * height * depth);
		
		delete[] t_buffer;

		pmat = matOpen(c_address, "w");
		matPutVariable(pmat, name, pa);
		matClose(pmat);
		
		mxFree(buffer);
	}
	
	
	///////////////////////////////////////////////////////////////////////////
	// Read MAT file to std::vector
	///////////////////////////////////////////////////////////////////////////
	
	template <typename TPixelType>
	std::vector< std::vector< TPixelType > > readMATFile_2D(
	const std::string& address){
	
		MATFile *pmat;
		mxArray *pa = 0;
		TPixelType* buffer;
		const char* c_address = address.c_str();
	
		pmat = matOpen(c_address, "r");
		pa = matGetVariable(pmat, "image");
		matClose(pmat);
	
		buffer = (TPixelType*)mxGetData(pa);
	
		size_t height = mxGetM(pa);
		size_t width = mxGetN(pa);
	
		typename std::vector< std::vector< TPixelType > > image(height,
		typename std::vector< TPixelType > (width));
	
		typename std::vector< std::vector< TPixelType > >::iterator irow;
		typename std::vector< TPixelType >::iterator icol;
	
		int i, j;
	
		for(i = 0, irow = image.begin() ; irow != image.end() ; irow++, i++){
			for(j = 0, icol = irow->begin() ; icol != irow->end() ; 
			icol++, j++)		
			*icol = buffer[i + j * height]; 
		}
	
		mxFree(buffer);
		return image;
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Write MAT file from std::vector
	///////////////////////////////////////////////////////////////////////////
	
	template <typename TPixelType>
	void writeMATFile_2D(const std::string& address, 
	const std::vector< std::vector< TPixelType > > image){
	
		int i, j;
		MATFile *pmat;
		mxArray *pa = 0;
		TPixelType* buffer;
		const char* c_address = address.c_str();	
	
		size_t height = image.size();
		size_t width = image[1].size();
	
		int t_int = 0;
		float t_float = 0;
		double t_double = 0;
	
		if(typeid(image[1][1]) == typeid(t_int))
			pa = mxCreateNumericMatrix(height, width, mxINT32_CLASS, mxREAL);
		else if(typeid(image[1][1]) == typeid(t_float))
			pa = mxCreateNumericMatrix(height, width, mxSINGLE_CLASS, mxREAL);
		else if(typeid(image[1][1]) == typeid(t_double))
			pa = mxCreateNumericMatrix(height, width, mxDOUBLE_CLASS, mxREAL);
		else
			std::cerr << "Data Type Not Supported" << std::endl;

		buffer = new TPixelType[height * width];
		buffer = (TPixelType*)mxGetData(pa);
	
		typename std::vector< std::vector< TPixelType > >::const_iterator irow;
		typename std::vector< TPixelType >::const_iterator icol;
	
		for(i = 0, irow = image.begin() ; irow != image.end() ; irow++, i++){
			for(j = 0, icol = irow->begin() ; icol != irow->end() ; icol++, 
			j++)		
			buffer[i + j * height] = *icol; 
		}
	
		pmat = matOpen(c_address, "w");
		matPutVariable(pmat, "image", pa);
		matClose(pmat);
	
		mxFree(buffer);
	}
	
	///////////////////////////////////////////////////////////////////////////
	// 2D std::vector to Buffer
	///////////////////////////////////////////////////////////////////////////
	
	
	template < typename TPixelType >
	TPixelType* Vec2Buffer(std::vector< std::vector< TPixelType > >
	 &image){

		long int height = image.size();
		long int width = image[0].size();

		TPixelType* buffer = new TPixelType[height * width];
		TPixelType* temp = buffer;
		
 		typename std::vector< std::vector< TPixelType > >::iterator irow;
 		typename std::vector< TPixelType >::iterator icol;
	
 		for(irow = image.begin() ; irow != image.end() ; irow++){
 			for(icol = irow->begin() ; icol != irow->end() ; icol++){		
 				*temp = *icol;
 				++temp; 
 			}
 			std::vector< TPixelType >().swap(*irow);
 		}
		return buffer;	
	}
	
	///////////////////////////////////////////////////////////////////////////
	// std::vector to Buffer
	///////////////////////////////////////////////////////////////////////////
	
	template < typename TPixelType >
	TPixelType* Vec2Buffer(std::vector< TPixelType > &image){

		long int width = image.size();
		TPixelType* buffer = new TPixelType[width];
		TPixelType* temp = buffer;
		
 		typename std::vector< TPixelType >::iterator icol;
	
 		for(icol = image.begin() ; icol != image.end() ; icol++){		
 			*temp = *icol;
 			++temp; 
 		}

		return buffer;	
	}

}


#endif

