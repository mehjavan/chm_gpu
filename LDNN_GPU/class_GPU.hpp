#ifndef CLASS_GPU_H_
#define CLASS_GPU_H_

#include "LDNN_Kernel.hpp"
#include <cuda_runtime.h>

class class_GPU{

private:
	
	int GPUID;
	int nGroup;
	int nDiscriminant;
	int nDimension;
	int batchSize;
	int blockSize;
	float* d_block_output;
	float* d_error;
	float* d_output;
	float* d_outputAND;
	float* d_term;
	float* d_updates;
	float* d_rearranged_updates;
	float* d_final_updates;
	float* d_discriminants;
	float* d_xtrain;
	float* d_ytrain;
	float* d_xtest;
	float* d_ytest;
	float* d_block_output_test;
	
public:
	
	class_GPU();
	class_GPU(int group, int discriminant, int dimension, int id);
	~class_GPU();
	void Allocate();
	void setGPUID(int id);
	int getGPUID(){return GPUID;};
	void setNGroup(int group){nGroup = group;};
	void setNDiscriminant(int discriminant){nDiscriminant = discriminant;};
	void setNDimension(int dimension){nDimension = dimension;};
	int getNGroup(){return nGroup;};
	int getNDiscriminant(){return nDiscriminant;};
	int getNDimension(){return nDimension;};
	void setTrainingData(float* h_xtrain, float* h_ytrain);
	void setTestingData(float* h_xtest);
	void getUpdates(float* h_updates, float* h_error);
	void getPredictions(float* h_predict);
	void setDiscriminants(float* h_discriminants);
	void getDiscriminants(float* h_discriminants);
	void processTraining(int batchNum, int nData);
	void processTesting();

};

///////////////////////////////////////////////////////////////////////////////

class_GPU::class_GPU(){
	
	GPUID = 0;
	nGroup = 0;
	nDiscriminant = 0;
	nDimension = 0;
	batchSize = BSIZE;
	blockSize = BLOCKSIZE;

	cudaSetDevice(GPUID);

	d_block_output = 0;
	d_error = 0;
	d_output = 0;
	d_outputAND = 0;
	d_term = 0;
	d_updates = 0;
	d_rearranged_updates = 0;
	d_final_updates = 0;
	d_discriminants = 0;
	d_xtrain = 0;
	d_ytrain = 0;
	d_xtest = 0;
	d_ytest = 0;
	d_block_output_test = 0;
	
}

///////////////////////////////////////////////////////////////////////////////

class_GPU::class_GPU(int group, int discriminant, int dimension, int id){
	
	GPUID = id;
	
	cudaSetDevice(GPUID);
	
	nGroup = group;
	nDiscriminant = discriminant;
	nDimension = dimension;
	batchSize = BSIZE;
	blockSize = BLOCKSIZE;
	
	cudaMalloc((void**)&d_block_output, sizeof(float) * nGroup * 
		nDiscriminant * (nDimension / blockSize + 1) * batchSize);
	cudaMalloc((void**)&d_error, sizeof(float) * batchSize);
	cudaMalloc((void**)&d_output, sizeof(float) * nGroup * nDiscriminant * 
		batchSize);
	cudaMalloc((void**)&d_outputAND, sizeof(float) * nGroup * batchSize);
	cudaMalloc((void**)&d_term, sizeof(float) * nGroup * batchSize);
	cudaMalloc((void**)&d_updates, sizeof(float) * nGroup * nDiscriminant * 
		nDimension* batchSize);
	cudaMalloc((void**)&d_rearranged_updates, sizeof(float) * nGroup * 
		nDiscriminant * nDimension * batchSize);
	cudaMalloc((void**)&d_final_updates, sizeof(float) * nGroup *
		nDiscriminant * nDimension * batchSize);
	cudaMalloc((void**)&d_discriminants, sizeof(float) * nGroup * 
		nDiscriminant * nDimension);
	cudaMalloc((void**)&d_xtrain, sizeof(float) * nDimension *
		batchSize);
	cudaMalloc((void**)&d_ytrain, sizeof(float) * batchSize);
	cudaMalloc((void**)&d_xtest, sizeof(float) * nDimension * MAX);
	cudaMalloc((void**)&d_ytest, sizeof(float) * MAX);
	cudaMalloc((void**)&d_block_output_test, sizeof(float) * nGroup *
		nDiscriminant * (nDimension / blockSize + 1) * MAX);
	
}

///////////////////////////////////////////////////////////////////////////////

class_GPU::~class_GPU(){
	
	cudaSetDevice(GPUID);

	cudaFree(d_block_output);
	cudaFree(d_error);
	cudaFree(d_output);
	cudaFree(d_outputAND);
	cudaFree(d_term);
	cudaFree(d_updates);
	cudaFree(d_rearranged_updates);
	cudaFree(d_final_updates);
	cudaFree(d_discriminants);
	cudaFree(d_xtrain);
	cudaFree(d_ytrain);
	cudaFree(d_xtest);
	cudaFree(d_ytest);
	cudaFree(d_block_output_test);
	
}

///////////////////////////////////////////////////////////////////////////////

void class_GPU::Allocate(){
	
	cudaSetDevice(GPUID);
	
	if(d_error != 0)
		return;
	
	cudaMalloc((void**)&d_block_output, sizeof(float) * nGroup * 
		nDiscriminant * (nDimension / blockSize + 1) * batchSize);
	cudaMalloc((void**)&d_error, sizeof(float) * batchSize);
	cudaMalloc((void**)&d_output, sizeof(float) * nGroup * nDiscriminant * 
		batchSize);
	cudaMalloc((void**)&d_outputAND, sizeof(float) * nGroup * batchSize);
	cudaMalloc((void**)&d_term, sizeof(float) * nGroup * batchSize);
	cudaMalloc((void**)&d_updates, sizeof(float) * nGroup * nDiscriminant * 
		nDimension * batchSize);
	cudaMalloc((void**)&d_rearranged_updates, sizeof(float) * nGroup * 
		nDiscriminant * nDimension * batchSize);
	cudaMalloc((void**)&d_final_updates, sizeof(float) * nGroup *
		nDiscriminant * nDimension * batchSize);
	cudaMalloc((void**)&d_discriminants, sizeof(float) * nGroup * 
		nDiscriminant * nDimension );
	cudaMalloc((void**)&d_xtrain, sizeof(float) * nDimension  *
		batchSize);
	cudaMalloc((void**)&d_ytrain, sizeof(float) * batchSize);
	cudaMalloc((void**)&d_xtest, sizeof(float) * nDimension  * MAX);
	cudaMalloc((void**)&d_ytest, sizeof(float) * MAX);
	cudaMalloc((void**)&d_block_output_test, sizeof(float) * nGroup *
		nDiscriminant * (nDimension / blockSize + 1) * MAX);
	
}

///////////////////////////////////////////////////////////////////////////////

void class_GPU::setTrainingData(float* h_xtrain, float* h_ytrain){
	
	cudaSetDevice(GPUID);

	cudaMemcpy(d_xtrain, h_xtrain, sizeof(float) * nDimension *
		batchSize, cudaMemcpyHostToDevice);
	cudaMemcpy(d_ytrain, h_ytrain, sizeof(float) * batchSize,
	cudaMemcpyHostToDevice);
	
}

///////////////////////////////////////////////////////////////////////////////

void class_GPU::setGPUID(int id){
	
	GPUID = id;
	cudaSetDevice(GPUID);
	
};

///////////////////////////////////////////////////////////////////////////////

void class_GPU::setTestingData(float* h_xtest){
	
	cudaSetDevice(GPUID);
	
	cudaMemcpy(d_xtest, h_xtest, sizeof(float) * nDimension *
			MAX, cudaMemcpyHostToDevice);
	
};

///////////////////////////////////////////////////////////////////////////////

void class_GPU::setDiscriminants(float* h_discriminants){
	
	cudaSetDevice(GPUID);
	
	cudaMemcpy(d_discriminants, h_discriminants, sizeof(float) *
		nGroup * nDiscriminant * nDimension,
			cudaMemcpyHostToDevice);
			
}

///////////////////////////////////////////////////////////////////////////////

void class_GPU::getDiscriminants(float* h_discriminants){
	
	cudaSetDevice(GPUID);
	
	cudaMemcpy(h_discriminants, d_discriminants, sizeof(float) *
		nGroup * nDiscriminant * nDimension,
			cudaMemcpyDeviceToHost);
			
}

///////////////////////////////////////////////////////////////////////////////

void class_GPU::getPredictions(float* h_predict){
	
	cudaSetDevice(GPUID);
	
	cudaMemcpy(h_predict, d_ytest, sizeof(float) * MAX,
	cudaMemcpyDeviceToHost);
	
}

///////////////////////////////////////////////////////////////////////////////

void class_GPU::getUpdates(float* h_updates, float* h_error){
	
	cudaSetDevice(GPUID);
	
	cudaMemcpy(h_error, d_error, sizeof(float) * batchSize,
	cudaMemcpyDeviceToHost);
	cudaMemcpy(h_updates, d_final_updates, sizeof(float) * nGroup *
		nDiscriminant * nDimension, cudaMemcpyDeviceToHost);
	
}

///////////////////////////////////////////////////////////////////////////////

void class_GPU::processTraining(int batchNum, int nData){
	
	cudaSetDevice(GPUID);
	
	LDNN_kernel_1 <<< dim3((nDimension / BLOCKSIZE) + 1, nGroup *
		nDiscriminant, batchSize), dim3(BLOCKSIZE, 1, 1)>>> (
		nGroup, nDiscriminant, nDimension, d_discriminants, d_xtrain, 
	d_block_output);

	LDNN_kernel_2 <<< dim3(1, 1, batchSize), dim3(1, nGroup * nDiscriminant, 1)>>> (
		nGroup, nDiscriminant, nDimension, d_ytrain, d_block_output, d_error, 
	d_output, d_outputAND, d_term);

	LDNN_kernel_3 <<< dim3((nDimension / BLOCKSIZE) + 1, nGroup * 
		nDiscriminant, batchSize), dim3(BLOCKSIZE, 1, 1)>>> (
		nGroup, nDiscriminant, nDimension, d_xtrain, d_error, d_output, 
	d_outputAND, d_term, d_updates);

	T_kernel <<< dim3((nDimension / TILE) + 1, nGroup * nDiscriminant, (batchSize /
		 TILE) + 1), dim3(TILE, 1, BLOCKROWS)>>> (
		d_updates, d_rearranged_updates, nDimension);

	LDNN_kernel_4 <<< dim3(1, nGroup * nDiscriminant, nDimension), dim3(batchSize,
	1, 1)>>> (
		nGroup, nDiscriminant, nDimension, d_rearranged_updates, 
	d_final_updates, batchNum, nData);
	
}

///////////////////////////////////////////////////////////////////////////////

void class_GPU::processTesting(){
	
	cudaSetDevice(GPUID);
	
	LDNN_kernel_1<<< dim3((nDimension / BLOCKSIZE) + 1, nGroup * 
		nDiscriminant , MAX), dim3(BLOCKSIZE, 1, 1)>>>(
			nGroup, nDiscriminant, nDimension, d_discriminants, d_xtest, 
				d_block_output_test);

	LDNN_kernel_5<<< dim3(1, 1, MAX), dim3(1, nGroup * nDiscriminant, 1)>>>(
		nGroup, nDiscriminant, nDimension, d_discriminants,
			d_xtest, d_block_output_test, d_ytest);
	
}

///////////////////////////////////////////////////////////////////////////////


#endif
