#include "class_GPU.hpp"
#include "vl/kmeans.h"
#include <time.h>
#include <math.h>
#include <iostream>
#include <vector>

////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////

template< typename T >
int shuffle(T* array1, int dim1, int size){

	T temp;
	double random;
	srand48( time(NULL) );

	for(long int i = 0 ; i < size ; i++){
		random = drand48();
		long int index = random * size; ;
		for(long int j = 0 ; j < dim1 ; j++){
			temp = array1[i * dim1 + j];
			array1[i * dim1 + j] = array1[index * dim1 + j];
			array1[index * dim1 + j] = temp;
		}
	}

	return 0;
}

////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////

template< typename T1, typename T2 >
int shuffle(T1* array1, T2* array2, int dim1, int dim2, int size){

	T1 temp1;
	T2 temp2;
	double random;
	srand48( time(NULL) );

	for(long int i = 0 ; i < size ; i++){
		random = drand48();
		long int index = random*size; ;
		for(long int j = 0 ; j < dim1 ; j++){
			temp1 = array1[i * dim1 + j];
			array1[i * dim1 + j] = array1[index * dim1 + j];
			array1[index * dim1 + j] = temp1;
		}
		for(long int j = 0 ; j<dim2 ; j++){
			temp2 = array2[i * dim2 + j];
			array2[i * dim2 + j] = array2[index * dim2 + j];
			array2[index * dim2 + j] = temp2;
		}
	}

	return 0;
}

////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////

float* initializeDiscriminants(float* xtrain, float* ytrain, int nDimension,
int nGroup, int nDiscriminant, int nData){

	shuffle(xtrain, ytrain, nDimension, 1, nData);

	long int num_P = 0;
	long int num_N = 0;
	long int downsampleFactor = 1;

	if(nData > 20000000)
		downsampleFactor = 30;
	else if(nData > 10000000)
		downsampleFactor = 20;
	else if(nData > 5000000)
		downsampleFactor = 10;
	else if(nData > 2000000)
		downsampleFactor = 5;
	else if(nData > 1000000)
		downsampleFactor = 2;



	for(long int i = 0 ; i < nData ; i++)
        	if(ytrain[i] == 0) num_N++; else num_P++;	

	float* c_P = (float*)vl_malloc(nGroup * nDimension * sizeof(float));
	float* c_N = (float*)vl_malloc(nDiscriminant * nDimension * sizeof(float));

	float* data_N;
	float* data_P;

	data_N = (float*)vl_malloc(((num_N / downsampleFactor) + 1) * nDimension *
		    sizeof(float));
	data_P = (float*)vl_malloc(((num_P / downsampleFactor) + 1) * nDimension *
		sizeof(float));

	num_P = 0;
	num_N = 0;

	int cnt_P = 0;
	int cnt_N = 0;

	for(long int i = 0 ; i < nData ; i++){
		if(ytrain[i] == 0){
			if(num_N%downsampleFactor == 0){
				for(long int j = 0 ; j < nDimension ; j++)
					data_N[cnt_N * nDimension + j] = xtrain[i * nDimension + j];
				cnt_N++;
			}
			num_N++;
		}
		else if(ytrain[i] == 1){
			if(num_P%downsampleFactor == 0){
				for(long int j = 0 ; j < nDimension ; j++)
					data_P[cnt_P * nDimension + j] = xtrain[i * nDimension + j];
				cnt_P++;
			}
			num_P++;
		}
	}
	
	std::cout << "Run Clustering ..." <<std::endl;
	clock_t t_start, t_end;
	t_start = clock();

	VlKMeans* kmeans_P;
	VlKMeans* kmeans_N;

	kmeans_P = vl_kmeans_new(VL_TYPE_FLOAT, VlDistanceL2);
	kmeans_N = vl_kmeans_new(VL_TYPE_FLOAT, VlDistanceL2);

	vl_kmeans_set_algorithm (kmeans_P, VlKMeansLloyd);
	vl_kmeans_set_algorithm (kmeans_N, VlKMeansLloyd);

	vl_kmeans_init_centers_plus_plus(kmeans_P, data_P, nDimension, cnt_P,
	nGroup);
	vl_kmeans_init_centers_plus_plus(kmeans_N, data_N, nDimension, cnt_N,
	nDiscriminant);

	vl_kmeans_set_max_num_iterations (kmeans_P, 100);
	vl_kmeans_set_max_num_iterations (kmeans_N, 100);

	vl_kmeans_refine_centers (kmeans_P, data_P, cnt_P);
	vl_kmeans_refine_centers (kmeans_N, data_N, cnt_N);

	t_end = clock();

	float time = float(t_end - t_start)/CLOCKS_PER_SEC;

	std::cout << "Done, it took " << time << " seconds!" << std::endl;

	c_P = (float*)vl_kmeans_get_centers(kmeans_P);
	c_N = (float*)vl_kmeans_get_centers(kmeans_N);

	vl_free(data_N);
	vl_free(data_P);

	float* h_discriminants = (float*)malloc(nGroup * nDiscriminant *
		(nDimension + 1) * sizeof(float));
	float* sum_discriminants = (float*)calloc(nGroup * nDiscriminant,
	    sizeof(float));
	float* bias = (float*)calloc(nGroup * nDiscriminant, sizeof(float));

	for(long int i = 0 ; i < nGroup ; i++){
		for(long int j = 0 ; j < nDiscriminant ; j++){
			for(long int k = 0 ; k < nDimension ; k++){
				h_discriminants[i * (nDimension + 1) * nDiscriminant + j *
					    (nDimension + 1) + k] = (c_P[i * nDimension + k] - c_N[j *
						    nDimension + k]);
				sum_discriminants[i * nDiscriminant + j] += powf((c_P[i *
					    nDimension + k] - c_N[j * nDimension + k]), 2);
				bias[i * nDiscriminant + j] -= (c_P[i * nDimension + k] +
					    c_N[j * nDimension + k]) * 0.5 * (c_P[i * nDimension + k] -
						    c_N[j * nDimension + k]);
			}
		}
	}

	for(long int i = 0 ; i < nGroup ; i++){
		for(long int j = 0 ; j < nDiscriminant ; j++){
			if(sqrt(sum_discriminants[i * nDiscriminant + j]) !=0 ){
				for(long int k = 0 ; k < nDimension + 1; k++){
					if(k == nDimension)
						h_discriminants[i * (nDimension + 1) * nDiscriminant +
							    j * (nDimension + 1) + k] = bias[i *
								    nDiscriminant + j] / sqrt(sum_discriminants[i *
									    nDiscriminant + j]);
					else
						h_discriminants[i * (nDimension + 1) * nDiscriminant +
							    j * (nDimension + 1) + k] /=
								    sqrt(sum_discriminants[i * nDiscriminant + j]);
				}
			}
			else{
				std::cout << "Warning:" << std::endl <<
					    "	There are zero initialized discriminants"
					    << " ,This may result in problems" << std::endl;
			}
		}
	}

	vl_free(c_N);
	vl_free(c_P);
	free(sum_discriminants);
	free(bias);

	return h_discriminants;
}
////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////

float* LDNN_train(float* xtrain, float* ytrain, int nDimension, int nData, 
int nGroup, int nDiscriminant, float epsilon, int moment, int nEpoch){

	int deviceCount;
	cudaGetDeviceCount(&deviceCount);

	std::cout << deviceCount << " GPU(s) were found on the root node" << 
    		std::endl;
	//deviceCount = 1;
	shuffle(xtrain, ytrain, nDimension, 1, nData);
	
	float* discriminants;
	discriminants = initializeDiscriminants(xtrain, ytrain, nDimension, 
	nGroup, nDiscriminant, nData); 

	float* updates = (float*)calloc((nDimension + 1) * nGroup * nDiscriminant,
	sizeof(float));
	float** prev_updates = (float**)malloc(deviceCount * sizeof(float*));
	
	for(int i = 0 ; i < deviceCount ; i++){
	  
	  	prev_updates[i] = (float*)calloc((nDimension + 1) * nGroup *
		nDiscriminant, sizeof(float));
	  
	}

	float* t_xtrain;
	float* t_ytrain;
	float* t_updates;
	float* t_error;
	float* t_discriminants;

	cudaMallocHost((void**)&t_xtrain, (nDimension + 1) * BSIZE * sizeof(float));
	cudaMallocHost((void**)&t_ytrain, BSIZE * sizeof(float));
	cudaMallocHost((void**)&t_updates, (nDimension + 1) * nGroup * nDiscriminant * sizeof(float));
	cudaMallocHost((void**)&t_discriminants, (nDimension + 1) * nGroup * nDiscriminant * sizeof(float));
	cudaMallocHost((void**)&t_error, BSIZE * sizeof(float));
	
	memcpy(t_discriminants, discriminants, sizeof(float) * (nDimension + 1) * nGroup * nDiscriminant);

	class_GPU **gpu;// = new class_GPU[deviceCount];
	gpu = new class_GPU*[deviceCount];

	for(int i = 0 ; i < deviceCount ; i++){
		gpu[i] = new class_GPU(nGroup, nDiscriminant, nDimension + 1 , i);
		//gpu[i].setGPUID(i);
		//gpu[i].setNGroup(nGroup);
		//gpu[i].setNDiscriminant(nDiscriminant);
		//gpu[i].setNDimension(nDimension + 1);
		//gpu[i].Allocate();
	}

	float error, time;
	time_t end1, start1 = clock();
	
	for(int e = 0 ; e < nEpoch ; e++){
		time_t end2, start2 = clock();
		shuffle(xtrain, ytrain, nDimension, 1, nData);
		std::cout << "Starting Epoch: " << e + 1 << std::endl;
		error = 0;
		for(long int i = 0 ; i < (nData / BSIZE / deviceCount) + 1 ; i++){
			for(long int k = 0 ; k < deviceCount ; k++){
				gpu[k]->setDiscriminants(t_discriminants);
				for(long int j = 0 ; j < BSIZE ; j++){				
					if(i * deviceCount * BSIZE + k * BSIZE + j < nData){
						t_ytrain[j] = ytrain[i * deviceCount * BSIZE + k *
						BSIZE + j];
						for(long int l = 0 ; l < nDimension + 1 ; l++){
							if(l == nDimension)
								t_xtrain[j * (nDimension + 1) + l] = 1;
							else

								t_xtrain[j * (nDimension + 1) + l] =
									xtrain[i * deviceCount * BSIZE * nDimension
										 + k * BSIZE * nDimension + j *
											 nDimension + l];
						}
					}
				}

				gpu[k]->setTrainingData(t_xtrain, t_ytrain);
				gpu[k]->processTraining(i, nData);
			}
			
			// Updating the discriminants and claculating epoch error

			for(long int k = 0 ; k < deviceCount ; k++){
				gpu[k]->getUpdates(t_updates, t_error);
				for(long int l = 0 ; l < (nDimension + 1) * nGroup * nDiscriminant ;
				l++){
					t_discriminants[l] += epsilon  * (moment * prev_updates[k][l] +
						t_updates[l]);
					prev_updates[k][l] = t_updates[l];
					//std::cout << i << " : device : " << k << " ----- " << t_updates[l] << std::endl;
				}
				for(long int l = 0 ; l < BSIZE ; l++){
					if(i * deviceCount * BSIZE + k * BSIZE + l < nData){
						error += powf(t_error[l], 2);
					}
				}
			}
		}
        end2 = clock();
        time = float(end2 - start2) / CLOCKS_PER_SEC;	
		std::cout << "Error for epoch " << e + 1 << " is: ----------> " << 
			sqrt(error / nData) << std::endl; 
		std::cout << "Epoch " << e + 1 << " is done, it took: " << 
			time << std::endl;
		
	}
	
	end1 = clock();
	time = double(end1 - start1)/CLOCKS_PER_SEC;
	std::cout << "Backpropagation is done, it took the overall time of: " <<
		time << " seconds." << std::endl;

	memcpy(discriminants, t_discriminants, (nDimension + 1) * nGroup * nDiscriminant * sizeof(float));	
		
	cudaFreeHost(t_xtrain);
	cudaFreeHost(t_ytrain);
	cudaFreeHost(t_updates);
	cudaFreeHost(t_error);
	cudaFreeHost(t_discriminants);

	free(updates);
	free(prev_updates);
	
	for(int i = 0 ; i < deviceCount ; i++){
		delete gpu[i];
	}
	
	delete[] gpu;
	

	return discriminants;	
}

////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////

float* LDNN_predict(float* xtest, int nDimension, int nData, int nGroup, 
int nDiscriminant, float* discriminants){
	
	int deviceCount;
	cudaGetDeviceCount(&deviceCount);

	float* h_xtest = new float[MAX * (nDimension + 1)];
	float* h_ytest = new float[MAX];
	float* output = new float[nData];
	
	/*cudaMallocHost((void**)&h_xtest, (nDimension + 1) * MAX * sizeof(float));
	cudaMallocHost((void**)&h_ytest, MAX * sizeof(float));
	*/
	
	class_GPU **gpu;// = new class_GPU[deviceCount];
	gpu = new class_GPU*[deviceCount];

	for(int i = 0 ; i < deviceCount ; i++){
		gpu[i] = new class_GPU(nGroup, nDiscriminant, nDimension + 1 , i);
		//gpu[i].setGPUID(i);
		//gpu[i].setNGroup(nGroup);
		//gpu[i].setNDiscriminant(nDiscriminant);
		//gpu[i].setNDimension(nDimension + 1);
		//gpu[i].Allocate();
	}

	for(int k = 0 ; k < deviceCount ; k++)
		gpu[k]->setDiscriminants(discriminants);

	for(long int i = 0 ; i < (nData / MAX / deviceCount) + 1 ; i++){
		for(long int k = 0 ; k < deviceCount ; k++){
			for(long int j = 0 ; j < MAX ; j++){
				if(i * deviceCount * MAX + k * MAX + j < nData){
					for(long int l = 0 ; l < nDimension + 1 ; l++){
						if(l == nDimension)
							h_xtest[j * (nDimension + 1) + l] = 1;
						else{
							h_xtest[j * (nDimension + 1) + l] =
								xtest[i * deviceCount * MAX * nDimension
									 + k * MAX * nDimension + j *
										 nDimension + l];

						}
					}
				}
			}
			gpu[k]->setTestingData(h_xtest);
			gpu[k]->processTesting();
		}

		// Getting predictions

		for(long int k = 0 ; k < deviceCount ; k++){

			//gpu[k]->synchronize();
			gpu[k]->getPredictions(h_ytest);

			for(long int j = 0 ; j < MAX ; j++){
				if(i * deviceCount * MAX + k * MAX + j < nData){
					output[i * deviceCount * MAX + k * MAX + j] = h_ytest[j];
				}
			}
		}
	}
	
	delete[] h_xtest;
	delete[] h_ytest;
	
	for(int i = 0 ; i < deviceCount ; i++){
		delete gpu[i];
	}
	
	delete[] gpu;
	
	
	return output;
}


