#ifndef LDNN_KERNEL_H_
#define LDNN_KERNEL_H_

#include <math.h>

#define BLOCKSIZE 128
#define BSIZE 256
#define MAX 1024
#define TILE 32
#define BLOCKROWS 8

///////////////////////////////////////////////////////////////////////////////
//
///////////////////////////////////////////////////////////////////////////////

__global__ void LDNN_kernel_1(int nGroup, int nDiscriminant, int nDimension,
 float* d_discriminants, float* d_xtrain, float* d_block_output) {

	__shared__ float temp[BLOCKSIZE];

	int l;
	temp[threadIdx.x] = 0;

	if(blockIdx.x * blockDim.x + threadIdx.x < nDimension){
		temp[threadIdx.x] = d_xtrain[blockIdx.x * blockDim.x + threadIdx.x + 
			blockIdx.z * nDimension] * d_discriminants[(blockIdx.x *
				 blockDim.x + threadIdx.x) + blockIdx.y * nDimension];
	}

	__syncthreads();

	if(threadIdx.x < BLOCKSIZE/2)
		temp[threadIdx.x] += temp[threadIdx.x + BLOCKSIZE/2];
	__syncthreads();

	if(threadIdx.x < BLOCKSIZE/4)
		temp[threadIdx.x] += temp[threadIdx.x + BLOCKSIZE/4];
	__syncthreads();

	if(threadIdx.x < BLOCKSIZE/8)
		temp[threadIdx.x] += temp[threadIdx.x + BLOCKSIZE/8];
	__syncthreads();

	if(threadIdx.x == 0){
		for(l = 1 ; l < BLOCKSIZE/8 ; l++)
			temp[0] += temp[l];
		d_block_output[blockIdx.x + blockIdx.y * (nDimension/BLOCKSIZE + 1) +
			 blockIdx.z * nDiscriminant * nGroup * (nDimension/BLOCKSIZE + 1)]
				  = temp[0];
	}

};

///////////////////////////////////////////////////////////////////////////////
//
///////////////////////////////////////////////////////////////////////////////

__global__ void LDNN_kernel_2(int nGroup, int nDiscriminant, int nDimension, 
float* d_ytrain, float* d_block_output, float* d_error, float* d_output, 
float* d_outputAND, float* d_term) {

	__shared__ float temp[MAX];

	int l;

	temp[threadIdx.y] = 0;
	for(l = 0 ; l < (nDimension/BLOCKSIZE + 1) ; l++){
		temp[threadIdx.y] += d_block_output[threadIdx.y * 
			(nDimension / BLOCKSIZE + 1) + l + nDiscriminant * nGroup * 
				(nDimension/BLOCKSIZE + 1) * blockIdx.z];
	}
	__syncthreads();
	temp[threadIdx.y] = 1 / (1 + expf(-temp[threadIdx.y]));
	d_output[threadIdx.y + nDiscriminant * nGroup * blockIdx.z] = 
		temp[threadIdx.y];

	__syncthreads();

	if((threadIdx.y % nDiscriminant) == 0){
		for(l = 1 ; l < nDiscriminant; l++){
			temp[threadIdx.y] *= temp[threadIdx.y + l];
		}

		d_outputAND[threadIdx.y / nDiscriminant + nGroup * blockIdx.z] = 
			temp[threadIdx.y];
		temp[threadIdx.y] = 1 - temp[threadIdx.y];
	}

	__syncthreads();

	if((threadIdx.y % nDiscriminant) == 0){
		d_term[threadIdx.y / nDiscriminant + nGroup * blockIdx.z] = 1;
		for(l = 0 ; l < nGroup ; l++){
			if( threadIdx.y / nDiscriminant != l)
				d_term[threadIdx.y / nDiscriminant + nGroup * blockIdx.z] *=
					temp[l * nDiscriminant];
		}
	}
	__syncthreads();

	if(threadIdx.y == 0){
		for(l = 1 ; l < nGroup ; l++){
			temp[threadIdx.y] *= temp[threadIdx.y + l * nDiscriminant];
		}

		temp[threadIdx.y] = 1 - temp[threadIdx.y];
		//d_error[blockIdx.z] = 0.1 + 0.8 * d_ytrain[blockIdx.z] - temp[threadIdx.y];
		d_error[blockIdx.z] = d_ytrain[blockIdx.z] - temp[threadIdx.y];
	}
	__syncthreads();
};

///////////////////////////////////////////////////////////////////////////////
//
///////////////////////////////////////////////////////////////////////////////

__global__ void LDNN_kernel_3(int nGroup, int nDiscriminant, int nDimension, 
float* d_xtrain, float* d_error, float* d_output, float* d_outputAND, 
float* d_term, float* d_updates) {

	__shared__ float error;
	__shared__ float output;
	__shared__ float outputAND;
	__shared__ float term ;

	error = d_error[blockIdx.z];
	output = d_output[blockIdx.y + blockIdx.z * nDiscriminant * nGroup];
	outputAND = d_outputAND[blockIdx.y / nDiscriminant + blockIdx.z * nGroup];
	term = d_term[blockIdx.y / nDiscriminant + blockIdx.z * nGroup];

	if((blockIdx.x * blockDim.x + threadIdx.x) < nDimension ){
		d_updates[(blockIdx.x * blockDim.x + threadIdx.x) + blockIdx.y * 
			nDimension * BSIZE + blockIdx.z * nDimension] = error * 
				(1 - output) * outputAND * term * d_xtrain[(blockIdx.x * 
					blockDim.x + threadIdx.x) + nDimension * blockIdx.z];
	}

};

///////////////////////////////////////////////////////////////////////////////
//
///////////////////////////////////////////////////////////////////////////////

__global__ void LDNN_kernel_4(int nGroup, int nDiscriminant, int nDimension, 
float* d_updates, float* d_final_updates, int level, int nData) {
  
	int l;
	__shared__ float temp[BSIZE];

	temp[threadIdx.x] = 0;

	if(level * BSIZE + threadIdx.x < nData){
		temp[threadIdx.x] = d_updates[blockIdx.y * nDimension * BSIZE + 
			blockIdx.z * BSIZE + threadIdx.x ];
	}
	__syncthreads();

	if(threadIdx.x<BSIZE/2)
		temp[threadIdx.x] += temp[threadIdx.x + BSIZE/2];
	__syncthreads();

	if(threadIdx.x<BSIZE/4)
		temp[threadIdx.x] += temp[threadIdx.x + BSIZE/4];
	__syncthreads();

	if(threadIdx.x<BSIZE/8)
		temp[threadIdx.x] += temp[threadIdx.x + BSIZE/8];
	__syncthreads();
	
	if(threadIdx.x<BSIZE/16)
		temp[threadIdx.x] += temp[threadIdx.x + BSIZE/16];
	__syncthreads();

	if(threadIdx.x == 0){
		for(l = 1 ; l < BSIZE/16 ; l++){
			temp[0] += temp[l];
		}
		d_final_updates[blockIdx.z + blockIdx.y * nDimension] = temp[0];
	}

};

///////////////////////////////////////////////////////////////////////////////
//
///////////////////////////////////////////////////////////////////////////////

__global__ void LDNN_kernel_5(int nGroup, int nDiscriminant, int nDimension, 
float* d_discriminants, float* d_xtrain, float* d_block_output, 
float* d_out) {

	__shared__ float temp[MAX];

	int l;

	temp[threadIdx.y] = 0;
	for(l = 0 ; l < (nDimension / BLOCKSIZE + 1) ; l++){
		temp[threadIdx.y] += d_block_output[threadIdx.y * (nDimension / 
			BLOCKSIZE + 1)+ l + nDiscriminant * nGroup * (nDimension / 
				BLOCKSIZE + 1) * blockIdx.z];
	}
	__syncthreads();

	temp[threadIdx.y] = 1 / (1 + expf(-temp[threadIdx.y]));

	__syncthreads();

	if((threadIdx.y%nDiscriminant)==0){

		for(l = 1 ; l < nDiscriminant; l++){
			temp[threadIdx.y] *= temp[threadIdx.y + l];
		}
		temp[threadIdx.y] = 1 - temp[threadIdx.y];
	}

	__syncthreads();

	if(threadIdx.y == 0){

		for(l = 1 ; l < nGroup ; l++){
			temp[threadIdx.y] *= temp[threadIdx.y + l * nDiscriminant];
		}

		d_out[blockIdx.z] = 1 - temp[threadIdx.y];
	}
};

///////////////////////////////////////////////////////////////////////////////
//
///////////////////////////////////////////////////////////////////////////////


__global__ void T_kernel(float* d_updates, float* d_rearranged_updates, 
int nDimension){

	__shared__ float temp[TILE][TILE + 1];

	int xIndex = blockIdx.x * TILE + threadIdx.x;
	int zIndex = blockIdx.z * TILE + threadIdx.z;
	int index_in = xIndex + zIndex * nDimension;

	for (int i = 0 ; i < TILE ; i += BLOCKROWS){
		if((zIndex + i < BSIZE) && (xIndex < nDimension))
			temp[threadIdx.z + i][threadIdx.x] = d_updates[index_in + i * 
				nDimension + blockIdx.y * BSIZE * nDimension];
	}
	__syncthreads();

	xIndex = blockIdx.z * TILE + threadIdx.x;
	zIndex = blockIdx.x * TILE + threadIdx.z;
	int index_out = xIndex + zIndex * BSIZE;

	for (int i = 0 ; i < TILE ; i += BLOCKROWS){
		if((zIndex + i < nDimension) && (xIndex < BSIZE))
			d_rearranged_updates[blockIdx.y * BSIZE * nDimension + index_out + 
				i * BSIZE] = temp[threadIdx.x][threadIdx.z + i];
	}

};


#endif

