// standard include files
#include <iostream>
#include <vector>
#include <typeinfo>
#include <algorithm>
#include <cstdio>
#include <ctime>
#include <sstream>
#include <string>
#include "itkImage.h"
#include "CHM.hpp"

int main(int argc, char* argv[]){
	
	CHMparam param;
	param.num = std::atoi(argv[3]); // Number of Training Images
	param.Nstage = std::atoi(argv[5]); // Number of Stages
	param.Nlevel = std::atoi(argv[6]); // Number of Levels
	
	std::string modelPath = argv[1]; // Directory of The Model to be Saved in
	std::string trainPath = argv[2]; // Directory of the Training Images
	std::string labelPath = argv[4]; // Directory of the Label Images
	std::string root;

	if(access(modelPath.c_str(), F_OK) == -1)
		mkdir(modelPath.c_str(), S_IRWXU | S_IRWXG | S_IRWXO);

	const int Dimension = 2;
	size_t dummy1, dummy2, dummy3;

	typedef itk::Image< float, Dimension > ImageType;
	std::vector< ImageType::Pointer > trainingImage(param.num);
	std::vector< ImageType::Pointer > labelImage(param.num);
	
	std::ostringstream temp;
	float* img;
		
	// Reading Training Images
	
	for(int i = 0 ; i < param.num ; i++){

		temp.str("");
		temp.clear();
		temp << "/" << i + 1 << ".mat";
		root = trainPath + temp.str();

		img = meh::readMATFileToBuffer< float, Dimension >(root, dummy1,
		dummy2, dummy3, "image");

		trainingImage[i] = meh::transferBufferToImage< float, Dimension >
		(img, dummy1, dummy2, dummy3);

		root = labelPath + temp.str();

		img = meh::readMATFileToBuffer< float, Dimension >(root, dummy1,
		dummy2, dummy3, "label");
		labelImage[i] = meh::transferBufferToImage< float, Dimension >
		(img, dummy1, dummy2, dummy3);
	}

	// Running trainCHM for each Level and Stage
	
	for(int s = 1 ; s < param.Nstage + 1 ; s++){
		param.stage = s;
		for(int l = 0 ; l < param.Nlevel + 1 ; l++){
			param.level = l;
			trainCHM< ImageType, float, Dimension >(trainingImage, labelImage, 
			modelPath, param);
			if(s == param.Nstage)
				break;
		}
	}
	
	// Starting the Testing Phase
	
	std::cout << "Start Testing ..." << std::endl;
		
	CHMparam t_param;
	t_param.num = std::atoi(argv[7]); // Number of Testing Images
	t_param.Nstage = std::atoi(argv[5]);
	t_param.Nlevel = std::atoi(argv[6]);

	typedef itk::Image< float, Dimension > ImageType;
	std::vector< ImageType::Pointer > testingImage(t_param.num);

	// Reading Testing Images
	
	for(int i = 0 ; i < t_param.num ; i++){

		temp.str("");
		temp.clear();
		temp << "/" << i + 1 + param.num << ".mat";
		root = trainPath + temp.str();
		
		img = meh::readMATFileToBuffer< float, Dimension >(root, dummy1,
		dummy2, dummy3, "image");

		testingImage[i] = meh::transferBufferToImage< float, Dimension >
		(img, dummy1, dummy2, dummy3);

	}
	
	// Running testCHM for each Level and Stage to Get Results
	
	for(int s = 1 ; s < t_param.Nstage + 1 ; s++){
		t_param.stage = s;
		for(int l = 0 ; l < t_param.Nlevel + 1 ; l++){
			t_param.level = l;
			std::cout << "Testing Level: " << l << ", Stage: " << s << std::endl;
			testCHM< ImageType, float, Dimension >(testingImage, modelPath, t_param);
			if(s == t_param.Nstage)
				break;
		}
	}

	return 0;
}
