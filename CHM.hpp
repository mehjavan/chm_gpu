#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <sys/stat.h>

#include "ImageIO.hpp"
#include "ImageAlgorithm.hpp"
#include "ImageFeature.hpp"
#include "LDNN_utils.hpp"

const int maxNumberOfSamples = 12000000;

////////////////////////////////////////////////////////////////////////////////
// Shuffling an Array
////////////////////////////////////////////////////////////////////////////////

template < typename T >
int shuffle(T* array1, int dim1, int size){

	T temp;
	double random;
	srand48( time(NULL) );

	for(long int i = 0 ; i < size ; i++){
		random = drand48();
		long int index = random * size; ;
		for(long int j = 0 ; j < dim1 ; j++){
			temp = array1[i * dim1 + j];
			array1[i * dim1 + j] = array1[index * dim1 + j];
			array1[index * dim1 + j] = temp;
		}
	}

	return 0;
}

////////////////////////////////////////////////////////////////////////////////
// Subsample 2 std::vectors
////////////////////////////////////////////////////////////////////////////////
	
template < typename TPixelType >
void subSample(std::vector< std::vector< TPixelType > > &matrix,
std::vector< TPixelType > &vector, const int numP, const int numN, 
std::vector< std::vector< TPixelType > > &n_matrix, std::vector
< TPixelType > &n_vector){
	
	int pixP = 0;
	int pixN = 0;
		
	int num;
	int cntP = 0;
	int cntN = 0;
	int cnt = 0;
	
	int* indexP;
	int* indexN;
		
	typename std::vector< TPixelType >::iterator it;
	
	if(numN + numP > maxNumberOfSamples){

		for(it = vector.begin() ; it != vector.end() ; ++it ){
			if(*it == 0)
				pixN++;
			else
				pixP++;		
		}
				
		indexP = new int[pixP];		
		indexN = new int[pixN];

		for(it = vector.begin() ; it != vector.end() ; ++it ){
			if(*it == 0){
				indexN[cntN] = cnt; 
				cntN++;
			}
			else{
				indexP[cntP] = cnt; 
				cntP++;
			}
			cnt++;		
		}
		
		num = static_cast<int>(maxNumberOfSamples * static_cast<float>
		(pixN + pixP) / static_cast<float>(numN + numP));

		int minP;
		int minN;
		
		if(pixN >= num / 2 && pixP >= num / 2){
			minP = num / 2;
			minN = num / 2;
		}
		else if(pixN >= num / 2 && pixP < num / 2){
			minP = pixP;
			minN = num - pixP;
		}
		else if(pixN < num / 2 && pixP >= num / 2){
			minN = pixN;
			minP = num - pixN;
		}
		std::vector< TPixelType > temp(matrix[0].size(), 0);
		n_matrix = std::vector< std::vector< TPixelType > >(minN + minP, temp);
		n_vector = std::vector< TPixelType >(minN + minP, 0);
		
		shuffle(indexP, 1, cntP);	
		shuffle(indexN, 1, cntN);
		
		for(int i = 0 ; i < minN ; i++){	
			n_matrix[i] = matrix[indexN[i]];
			n_vector[i] = vector[indexN[i]];
		}
		
		for(int i = 0 ; i < minP ; i++){
			n_matrix[i + minN] = matrix[indexP[i]];
			n_vector[i + minN] = vector[indexP[i]];
		}
		
		delete[] indexN;
		delete[] indexP;

	}
	else{
		n_matrix = matrix;
		n_vector = vector;
	}


}

////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////

struct CHMparam{

	int Nstage;
	int Nlevel;
	int stage;
	int level;
	int num;

};

////////////////////////////////////////////////////////////////////////////////
// Learning Classifier Parameters
////////////////////////////////////////////////////////////////////////////////

void learnClassifier(CHMparam param, int &nGroup, int &nDiscriminant, 
float &epsilon, float &moment, int &nEpoch){

	nEpoch = 20;
	epsilon = 0.01;
	moment = 0.5;
	nGroup = 8;
	nDiscriminant = 8;

	if(param.level == 0){
		epsilon = 0.01;
		moment = 0.5;
		if(param.stage == 1)
			nEpoch = 20;
		else{
			nEpoch = 15;
			epsilon = 0.01;
		}
		nGroup = 16;
		nDiscriminant = 16;
	}

	if(param.level == 1){
		epsilon = 0.01;
		moment = 0.5;
		nEpoch = 20;
		nGroup = 12;
		nDiscriminant = 12;
	}

	if(param.level == 2){
		epsilon = 0.01;
		moment = 0.5;
		nEpoch = 20;
		nGroup = 10;
		nDiscriminant = 10;
	}
	
}

////////////////////////////////////////////////////////////////////////////////
// Training a Classifier for a Specific Stage and Level
////////////////////////////////////////////////////////////////////////////////

template < typename TImageType, typename TPixelType = float, int Dimension = 2 >
int trainCHM(const std::vector< typename TImageType::Pointer > trainingImage ,
		const std::vector< typename TImageType::Pointer > labelImage,
		const std::string &modelPath,
		CHMparam param){
	
	bool flag_learn = false;
	bool flag_predict = false;
	
	std::string root;
	std::ostringstream model_loc;
	std::ostringstream data_loc;
	std::ostringstream temp;
	
	data_loc << modelPath << "/Outputs_level" << param.level << "_stage" 
		<< param.stage;
	
	model_loc << modelPath << "/Model_level" << param.level << "_stage" 
		<< param.stage << ".mat";
	
	if(access(model_loc.str().c_str(), F_OK) == -1)
		flag_learn = true;
	
	if(access(data_loc.str().c_str(), F_OK) == -1)
		mkdir(data_loc.str().c_str(), S_IRWXU | S_IRWXG | S_IRWXO);
	
	for(int i = 0 ; i < param.num ; i++){
		temp.str("");
		temp.clear();
		temp << "/Slice" << i + 1 << ".mat";
		root = data_loc.str() + temp.str();
		if(access(root.c_str(), F_OK) == -1){
			flag_predict = true;
			break;
		}
	}

	// Feature Extraction and Training the Classifier
	
	if(flag_learn){
		
		int numP = 0;
		int numN = 0;
		
		float* discriminants;
	
		std::vector< TPixelType > ytrain;
		std::vector< std::vector< TPixelType > > xtrain;
		std::vector< std::vector< TPixelType > > imageFeat;
		std::vector< std::vector< TPixelType > > s_imageFeat;
		std::vector< std::vector< TPixelType > > contextFeat;
		std::vector< TPixelType > imageLabel;
		std::vector< TPixelType > s_imageLabel;
		
		typename TImageType::Pointer image;
		typename TImageType::Pointer label;
		typename TImageType::Pointer tempImage;
		typename TImageType::RegionType region;
		typename TImageType::SizeType size;
		
		// Finding Positive and Negative Samples for Subsampling if Necessary

		for(int i = 0 ; i < param.num ; i++){

			imageLabel = meh::transferImageTo1DVector
					< TPixelType, Dimension >(labelImage[i]);
			
			typename std::vector< TPixelType >::iterator it;
			
			for(it = imageLabel.begin() ; it != imageLabel.end() ; ++it )
				if(*it == 0)
					numN++;
				else
					numP++;	
		
		}
		
		// Feature Extraction
		
		std::cout << "Extracting Features: Stage: " << param.stage << 
			" Level: " << param.level << std::endl;
				
		if( param.stage == 1 || param.level != 0 ){
			
			// Bottom - Up Level
			
			for(int i = 0 ; i < param.num ; i++){
				
				// Getting the Size of The original Image
			  
				size_t dummy1, dummy2, dummy3;
				region = trainingImage[i]->GetLargestPossibleRegion();
				size = region.GetSize();

				// Finding Size of Images at the Current Level
				
				int n_width = int((size[0] + pow(2, param.level) - 1) /
					 pow(2, param.level));
				int n_height = int((size[1] + pow(2, param.level) - 1) /
						 pow(2, param.level));
				int n_depth = 1;
				if(Dimension == 3)
					n_depth = int((size[2] + pow(2, param.level) - 1) /
							 pow(2, param.level));
				
				// Resizing the Images to the Desired Size at the Current Level	
					
				image = meh::resizeImage< TPixelType, Dimension >
					(trainingImage[i], n_height, n_width, n_depth);
					
				// Extracting Features from the Resized Image	
				
				imageFeat = meh::filterBankImage< TPixelType >(image);
				
				// Extracting Context Features from Previous Levels
				
				contextFeat.clear();
				
				for(int j = 0 ; j < param.level ; j++){
				
					float* t_buffer;
					
					// Finding the Size of the Image in the Previous Levels
					
					int p_width = int((size[0] + pow(2, j) - 1) / pow(2, j));
					int p_height = int((size[1] + pow(2, j) - 1) / pow(2, j));
					int p_depth = 1;
					if(Dimension == 3)
						p_depth = int((size[2] + pow(2, j) - 1) / pow(2, j));
					
					// Reading the Output from Previous Levels

					temp.str("");
					temp.clear();
					temp << modelPath << "/Outputs_level" << j << "_stage" << 
						param.stage << "/Slice" << i + 1 << ".mat";
					root = temp.str();
					
					t_buffer = meh::readMATFileToBuffer< TPixelType >
								(root, dummy1, dummy2, dummy3, "clabels");
								
					// Resizing the Outputs from Previous Levels to the 
					// Desired Size at the Current Level			
					
					tempImage = meh::resizeImage< TPixelType, Dimension >
						(meh::transferBufferToImage< TPixelType, Dimension>
							(t_buffer,  p_height, p_width, p_depth), 
								 n_height, n_width, n_depth);
						
					// Extracting Context Features	
																			
					meh::insertVector< TPixelType >(contextFeat, 
					meh::contextFilterBankImage< TPixelType, Dimension>
						(tempImage));
					
					delete[] t_buffer;
				}
				
				// Concatenating the Two Feature Vectors into One	
				
				meh::insertVector< TPixelType >(imageFeat, contextFeat);
				
				// Maxpooling the Label Image
				
				label = meh::maxPoolImage< TPixelType, Dimension >
					(labelImage[i], param.level);
				
				imageLabel = meh::transferImageTo1DVector
					< TPixelType, Dimension >(label);

				// Subsampling the Feature Vector if Necessary	
					
				subSample< TPixelType >(imageFeat, imageLabel, numP, numN,
				s_imageFeat, s_imageLabel);
				
				// Concatenating the Feature Vector for this Image 
				// to the Main Feature Vector

				meh::catVector< TPixelType >(xtrain, s_imageFeat);
				
				ytrain.insert(ytrain.end(), s_imageLabel.begin(), 
				s_imageLabel.end());

			}
		}
		else{
			
			// Top - Down level	
				
			for(int i = 0 ; i < param.num ; i++){
				
				// Getting the Size of The original Image
			  
				size_t dummy1, dummy2, dummy3;
				region = trainingImage[i]->GetLargestPossibleRegion();
				size = region.GetSize();
				
				// Finding Size of Images at the Current Level
		
				int n_width = int((size[0] + pow(2, param.level) - 1) /
					 pow(2, param.level));
				int n_height = int((size[1] + pow(2, param.level) - 1) /
						 pow(2, param.level));
				int n_depth = 1;
				if(Dimension == 3)
					n_depth = int((size[2] + pow(2, param.level) - 1) /
							 pow(2, param.level));
					
				// Resizing the Images to the Desired Size at the Current Level		
			
				image = meh::resizeImage< TPixelType, Dimension >
					(trainingImage[i], n_height, n_width, n_depth);
					
				// Extracting Features from the Resized Image
					
				imageFeat = meh::filterBankImage< TPixelType >
					(image);
				
				// Extracting Context Features from Previous Levels	
					
				contextFeat.clear();
				
				for(int j = 0 ; j < param.Nlevel + 1 ; j++){
				
					float* t_buffer;
					
					// Finding the Size of the Image in the Previous Levels
					
					int p_width = int((size[0] + pow(2, j) - 1) / pow(2, j));
					int p_height = int((size[1] + pow(2, j) - 1) / pow(2, j));
					int p_depth = 1;
					if(Dimension == 3)
						p_depth = int((size[2] + pow(2, j) - 1) / pow(2, j));
					
					// Reading the Output from Previous Levels
					
					temp.str("");
					temp.clear();
					temp << modelPath << "/Outputs_level" << j << "_stage" << 
						param.stage - 1 << "/Slice" << i + 1 << ".mat";
					root = temp.str();
					
					t_buffer = meh::readMATFileToBuffer< TPixelType >
								(root, dummy1, dummy2, dummy3, "clabels");
								
					// Resizing the Outputs from Previous Levels to the 
					// Desired Size at the Current Level	
					
					tempImage = meh::resizeImage< TPixelType, Dimension >
						(meh::transferBufferToImage< TPixelType, Dimension>
							(t_buffer,  p_height, p_width, p_depth), 
								 n_height, n_width, n_depth);
						
					// Extracting Context Features	
																			
					meh::insertVector< TPixelType >(contextFeat, 
					meh::contextFilterBankImage< TPixelType, Dimension>
						(tempImage));
					
					delete[] t_buffer;				
				}
				
				// Concatenating the Two Feature Vectors into One
				
				meh::insertVector< TPixelType >(imageFeat, contextFeat);
				
				// Maxpooling the Label Image
				
				label = meh::maxPoolImage< TPixelType, Dimension >
					(labelImage[i], param.level);
				
				imageLabel = meh::transferImageTo1DVector
					< TPixelType, Dimension >(label);

				// Subsampling the Feature Vector if Necessary	
					
				subSample< TPixelType >(imageFeat, imageLabel, numP, numN,
				s_imageFeat, s_imageLabel);
				
				// Concatenating the Feature Vector for this Image 
				// to the Main Feature Vector
				
				meh::catVector< TPixelType >(xtrain, s_imageFeat);
				ytrain.insert(ytrain.end(), s_imageLabel.begin(), 
				s_imageLabel.end());
				
			}
		}
		
		// Learning the Classifier Parameters
		
		int nGroup = 0;
		int nDiscriminant = 0;
		int nEpoch = 0;
		float epsilon = 0;
		float moment = 0;
		
		learnClassifier(param, nGroup, nDiscriminant, epsilon, moment, nEpoch);
		
		std::cout << "Start Learning the Classifier ..." << std::endl;
		
		size_t nData = xtrain.size();
		size_t nDimension = xtrain[0].size();

		float* xxtrain = meh::Vec2Buffer< float >(xtrain);
		float* yytrain = meh::Vec2Buffer< float >(ytrain);
		
		std::cout << "Number of Samples to be Trained: " << nData << std::endl;
		std::cout << "Dimension of the Samples:  " << nDimension << std::endl;
	
		// Calling the Classifier 
		
		discriminants = LDNN_train(xxtrain, yytrain, nDimension, 
		nData, nGroup, nDiscriminant, epsilon, moment, nEpoch);
		
		delete[] xxtrain;
		delete[] yytrain;
		
		meh::writeBufferToMATFile_NT< TPixelType >(model_loc.str(), discriminants,
		 "discriminants", nDimension + 1, nGroup * nDiscriminant);
		
		std::cout << "Learning is Done!" << std::endl;
		free(discriminants);
	}
	
// Generating predicted outputs

	if(flag_predict){
	
		std::cout << "Generating Outputs: Stage: " << param.stage << 
			" Level: " << param.level << std::endl;
		
		int nGroup = 0;
		int nDiscriminant = 0;
		int nEpoch = 0;
		float epsilon = 0;
		float moment = 0;
		
		size_t dummy1, dummy2, dummy3;
		float* predict;
		std::vector< std::vector< TPixelType > > imageFeat;
		std::vector< std::vector< TPixelType > > contextFeat;
	
		typename TImageType::Pointer image;
		typename TImageType::Pointer tempImage;
		typename TImageType::RegionType region;
		typename TImageType::SizeType size;
		
		float* discriminants;

		discriminants = meh::readMATFileToBuffer_NT< TPixelType >
			(model_loc.str(), dummy1, dummy2, dummy3, "discriminants");
		
		for(int i = 0 ; i < param.num ; i++){
		
			temp.str("");
			temp.clear();
			temp << modelPath << "/Outputs_level" << param.level << 
			"_stage" << param.stage << "/Slice" << i + 1 << ".mat";
			root = temp.str();
			
			if(access(root.c_str(), F_OK) == 0){
				continue;
			}
			
			size_t dummy1, dummy2, dummy3;
			region = trainingImage[i]->GetLargestPossibleRegion();
			size = region.GetSize();
	
			int n_width = int((size[0] + pow(2, param.level) - 1) /
				 pow(2, param.level));
			int n_height = int((size[1] + pow(2, param.level) - 1) /
					 pow(2, param.level));
			int n_depth = 1;
			if(Dimension == 3)
				n_depth = int((size[2] + pow(2, param.level) - 1) /
						 pow(2, param.level));
		
			if( param.stage == 1 || param.level != 0 ){
		
				image = meh::resizeImage< TPixelType, Dimension >
					(trainingImage[i], n_height, n_width, n_depth);
				imageFeat = meh::filterBankImage< TPixelType >
					(image);
					
				contextFeat.clear();	
			
				for(int j = 0 ; j < param.level ; j++){
				
					float* t_buffer;
				
					int p_width = int((size[0] + pow(2, j) - 1) / pow(2, j));
					int p_height = int((size[1] + pow(2, j) - 1) / pow(2, j));
					int p_depth = 1;
					if(Dimension == 3)
						p_depth = int((size[2] + pow(2, j) - 1) / pow(2, j));
				
					temp.str("");
					temp.clear();
					temp << modelPath << "/Outputs_level" << j << "_stage" << 
						param.stage << "/Slice" << i + 1 << ".mat";
					root = temp.str();
				
					t_buffer = meh::readMATFileToBuffer< TPixelType >
								(root, dummy1, dummy2, dummy3, "clabels");
					
					tempImage = meh::resizeImage< TPixelType, Dimension >
						(meh::transferBufferToImage< TPixelType, Dimension>
							(t_buffer, p_height, p_width, p_depth) 
								, n_height, n_width, n_depth);
																			
					meh::insertVector< TPixelType >(contextFeat, 
					meh::contextFilterBankImage< TPixelType, Dimension>
						(tempImage));
					
					delete[] t_buffer;
				
				}
			
				meh::insertVector< TPixelType >(imageFeat, contextFeat);
			
			}
			else{
				
				image = meh::resizeImage< TPixelType, Dimension >
					(trainingImage[i], n_height, n_width, n_depth);
				imageFeat = meh::filterBankImage< TPixelType >
					(image);
					
				contextFeat.clear();	
					  
				for(int j = 0 ; j < param.Nlevel + 1 ; j++){
				
					float* t_buffer;
				
					int p_width = int((size[0] + pow(2, j) - 1) / pow(2, j));
					int p_height = int((size[1] + pow(2, j) - 1) / pow(2, j));
					int p_depth = 1;
					if(Dimension == 3)
						p_depth = int((size[2] + pow(2, j) - 1) / pow(2, j));
				
					temp.str("");
					temp.clear();
					temp << modelPath << "/Outputs_level" << j << "_stage" << 
						param.stage - 1 << "/Slice" << i + 1 << ".mat";
					root = temp.str();
					
					t_buffer = meh::readMATFileToBuffer< TPixelType >
								(root, dummy1, dummy2, dummy3, "clabels");
					
					tempImage = meh::resizeImage< TPixelType, Dimension >
						(meh::transferBufferToImage< TPixelType, Dimension>
							(t_buffer, p_height, p_width, p_depth) 
								, n_height, n_width, n_depth);
								
					meh::insertVector< TPixelType >(contextFeat, 
					meh::contextFilterBankImage< TPixelType, Dimension>
						(tempImage));
					
					delete[] t_buffer;
				}
				
				meh::insertVector< TPixelType >(imageFeat, contextFeat);
			
			}
					
			learnClassifier(param, nGroup, nDiscriminant, epsilon, moment, 
			nEpoch);
			
			size_t nData = imageFeat.size();
			size_t nDimension = imageFeat[0].size();
			float* xtest = meh::Vec2Buffer(imageFeat);
			
			// Generating the Outputs
			
			predict = LDNN_predict(xtest, nDimension,
			nData, nGroup, nDiscriminant, discriminants);
			
			delete[] xtest;
			
			temp.str("");
			temp.clear();
			temp << modelPath << "/Outputs_level" << param.level << 
			"_stage" << param.stage << "/Slice" << i + 1 << ".mat";
			root = temp.str();
			
			meh::writeBufferToMATFile< TPixelType >(root, predict, "clabels", 
			n_height, n_width, n_depth);
			
			//meh::writeImage<TImageType>(meh::transferBufferToImage< TPixelType >(predict, n_height, n_width, n_depth), root);
			delete[] predict;

		}
		delete[] discriminants;
	}
	
	return 0;
}

////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////

template < typename TImageType, typename TPixelType = float, int Dimension = 2 >
int testCHM(const std::vector< typename TImageType::Pointer > testingImage,
const std::string &modelPath, CHMparam param){
	
	std::string root;
	std::ostringstream model_loc;
	std::ostringstream data_loc;
	std::ostringstream temp;

	data_loc << modelPath << "/Test_Outputs_level" << param.level << "_stage"
		<< param.stage;

	model_loc << modelPath << "/Model_level" << param.level << "_stage"
		<< param.stage << ".mat";

	if(access(data_loc.str().c_str(), F_OK) == -1)
		mkdir(data_loc.str().c_str(), S_IRWXU | S_IRWXG | S_IRWXO);

	int nGroup = 0;
	int nDiscriminant = 0;
	int nEpoch = 0;
	float epsilon = 0;
	float moment = 0;
	size_t dummy1, dummy2, dummy3;
	
	float* predict;
	std::vector< std::vector< TPixelType > > imageFeat;
	std::vector< std::vector< TPixelType > > contextFeat;

	typename TImageType::Pointer image;
	typename TImageType::Pointer tempImage;
	typename TImageType::RegionType region;
	typename TImageType::SizeType size;
	
	float* discriminants;
	discriminants = meh::readMATFileToBuffer_NT< TPixelType >
		(model_loc.str(), dummy1, dummy2, dummy3, "discriminants");
		
	std::cout << "Testing, Stage: " << param.stage << ", Level: " << param.level << std::endl;	
	
	for(int i = 0 ; i < param.num ; i++){
	
		temp.str("");
		temp.clear();
		temp << modelPath << "/Test_Outputs_level" << param.level << "_stage"
			 <<	param.stage << "/Slice" << i + 1 << ".mat";
		root = temp.str();
			
		if(access(root.c_str(), F_OK) == 0){
			continue;
		}

		region = testingImage[i]->GetLargestPossibleRegion();
		size = region.GetSize();

		int n_width = int((size[0] + pow(2, param.level) - 1) /
			 pow(2, param.level));
		int n_height = int((size[1] + pow(2, param.level) - 1) /
				 pow(2, param.level));
		int n_depth = 1;
		if(Dimension == 3)
			n_depth = int((size[2] + pow(2, param.level) - 1) /
					 pow(2, param.level));
	
		if( param.stage == 1 || param.level != 0 ){
	
			image = meh::resizeImage< TPixelType, Dimension >
				(testingImage[i], n_height, n_width, n_depth);
			imageFeat = meh::filterBankImage< TPixelType >
				(image);
		
			contextFeat.clear();	
			
			for(int j = 0 ; j < param.level ; j++){
			
				float* t_buffer; 
			
				int p_width = int((size[0] + pow(2, j) - 1) / pow(2, j));
				int p_height = int((size[1] + pow(2, j) - 1) / pow(2, j));
				int p_depth = 1;
				if(Dimension == 3)
					p_depth = int((size[2] + pow(2, j) - 1) / pow(2, j));
			
				temp.str("");
				temp.clear();
				temp << modelPath << "/Test_Outputs_level" << j << "_stage" << 
					param.stage << "/Slice" << i + 1 << ".mat";
				root = temp.str();
				
				t_buffer = meh::readMATFileToBuffer< TPixelType >
								(root, dummy1, dummy2, dummy3, "clabels");									
					
				tempImage = meh::resizeImage< TPixelType, Dimension >
					(meh::transferBufferToImage< TPixelType, Dimension>
						(t_buffer, p_height, p_width, p_depth) 
							, n_height, n_width, n_depth);
																			
				meh::insertVector< TPixelType >(contextFeat, 
				meh::contextFilterBankImage< TPixelType, Dimension>
						(tempImage));
					
				delete[] t_buffer;						
			}
		
			meh::insertVector< TPixelType >(imageFeat, contextFeat);
		
		}
		else{
			
			image = meh::resizeImage< TPixelType, Dimension >
				(testingImage[i], n_height, n_width, n_depth);
			imageFeat = meh::filterBankImage< TPixelType >
				(image);
				
			contextFeat.clear();	
		
			for(int j = 0 ; j < param.Nlevel + 1 ; j++){
			
				float* t_buffer;
			
				int p_width = int((size[0] + pow(2, j) - 1) / pow(2, j));
				int p_height = int((size[1] + pow(2, j) - 1) / pow(2, j));
				int p_depth = 1;
				if(Dimension == 3)
					p_depth = int((size[2] + pow(2, j) - 1) / pow(2, j));
			
				temp.str("");
				temp.clear();
				temp << modelPath << "/Test_Outputs_level" << j << "_stage" << 
					param.stage - 1 << "/Slice" << i + 1 << ".mat";
				root = temp.str();
			
				t_buffer = meh::readMATFileToBuffer< TPixelType >
							(root, dummy1, dummy2, dummy3, "clabels");
					
				tempImage = meh::resizeImage< TPixelType, Dimension >
					(meh::transferBufferToImage< TPixelType, Dimension>
						(t_buffer, p_height, p_width, p_depth) 
							, n_height, n_width, n_depth);
																			
				meh::insertVector< TPixelType >(contextFeat, 
				meh::contextFilterBankImage< TPixelType, Dimension>
					(tempImage));
					
				delete[] t_buffer;;
			}
		
			meh::insertVector< TPixelType >(imageFeat, contextFeat);
			
		}
				
		learnClassifier(param, nGroup, nDiscriminant, epsilon, moment, 
		nEpoch);
		
		size_t nData = imageFeat.size();
		size_t nDimension = imageFeat[0].size();
		float* xtest = meh::Vec2Buffer(imageFeat);
			
		predict = LDNN_predict(xtest, nDimension,
		nData, nGroup, nDiscriminant, discriminants);
			
		delete[] xtest;
		
		temp.str("");
		temp.clear();
		temp << modelPath << "/Test_Outputs_level" << param.level << "_stage"
			 <<	param.stage << "/Slice" << i + 1 << ".mat";
		root = temp.str();
		
		meh::writeBufferToMATFile< TPixelType >(root, predict, "clabels", 
		n_height, n_width, n_depth);
		
	}
	
	
}


