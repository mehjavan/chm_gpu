This is a GPU implementation of the Cascaded Hierarchical Model (CHM). For More information on CHM refer to [*Image segmentation with cascaded hierarchical models and logistic disjunctive normal networks (ICCV 2013)*](http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=6751380&tag=1).

To get this code running you will need the following libraries.

1 - MATLAB Libraries. If you have MATLAB installed on your system, you would be all set. The binaries are found in *MATLAB-dir/bin/glnxa64* and the include files are in *MATLAB-dir/extern/include* (I am using Linux, it may be a bit different on your system).

2 - ITK Library. It should be built using the *-std=c++0x* flag. Also you may need to set the ItkVtkGlue flag to on when you are building the library. To do so you may also need to install the VTK library as well (It is recommended that you install VTK first, then ITK), These libraries are found in http://www.itk.org/ and http://www.vtk.org/.

3 - VLFeat Library. You can download this library from http://www.vlfeat.org/.

4 - And of course you need CUDA.

You should set the correct file paths in the CMakeLists.txt according to your system and where you installed the libraries.